package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.model.Sport

class CoachDataSource : TableDataSource<Coach>(Coach.VIEW, parse = {
    var columnIndex = 1
    Coach(
        getInt(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        Sport(
            getInt(columnIndex++),
            getString(columnIndex)
        )
    )
})