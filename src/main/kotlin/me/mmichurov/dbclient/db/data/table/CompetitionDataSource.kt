package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Competition
import me.mmichurov.dbclient.db.model.CompetitionOrganizer
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportsFacility

class CompetitionDataSource : TableDataSource<Competition>(Competition.VIEW, parse = {
    var columnIndex = 1
    Competition(
        getInt(columnIndex++),
        getString(columnIndex++),
        getDate(columnIndex++).toLocalDate(),
        Sport(
            getInt(columnIndex++),
            getString(columnIndex++),
        ),
        CompetitionOrganizer(
            getInt(columnIndex++),
            getString(columnIndex++)
        ),
        SportsFacility(
            getInt(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex)
        )
    )
})