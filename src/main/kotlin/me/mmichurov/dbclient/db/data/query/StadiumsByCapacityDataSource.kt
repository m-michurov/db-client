package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Stadium

class StadiumsByCapacityDataSource : QueryDataSource<Stadium>(
    """
        select
            "Name",
            "Address",
            "Capacity"
        from
            "Stadium_View"
        where
            "Capacity" >= :capacity
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        Stadium(
            0,
            getString(columnIndex++),
            getString(columnIndex++),
            getInt(columnIndex)
        )
    }
)