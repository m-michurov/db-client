package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.*
import java.time.LocalDate

class CompetitorsDataSource : QueryDataSource<Competitor>(
    """
        select
            "First_Name",
            "Last_Name",
            "Place"
        from
            "Competitors_View"
        where 
            "Competition_Id" = :competition_id
    """.trimIndent(),
    parse = {
    var columnIndex = 1
    Competitor(
        null,
        Sportsman(
            0,
            getString(columnIndex++),
            getString(columnIndex++),
            null,
            LocalDate.EPOCH
        ),
        getString(columnIndex)
    )
})