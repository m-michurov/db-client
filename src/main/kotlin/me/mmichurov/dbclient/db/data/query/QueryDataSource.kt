package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.data.DataSource
import tornadofx.runLater
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

abstract class QueryDataSource<T : DataAccessObject>(
    queries: Map<String, String>,
    private val parse: ResultSet.() -> T
) : DataSource<T>() {

    // It's lazy-initialized for a reason
    // I just don't remember what it was
    private val statements = queries
        .map { (id, query) -> id to db.prepare(query) }
        .toMap()

    constructor(
        query: String,
        parse: ResultSet.() -> T
    ) : this(mapOf(DEFAULT_QUERY to query), parse)

    private fun fetch(id: String, applyParameters: PreparedStatement.() -> Unit): List<T> =
        statements[id]?.let {
            db.fetch(it, applyParameters, parse)
        } ?: run {
            throw NotImplementedError("Query \"$id\" is not implemented")
        }

    /**
     * @throws SQLException
     * @throws NotImplementedError
     */
    fun query(
        id: String = DEFAULT_QUERY,
        applyParameters: (PreparedStatement) -> Unit
    ) = fetch(id, applyParameters).let {
        runLater { items.setAll(it) }
    }

    companion object {

        const val DEFAULT_QUERY = "default"
    }
}