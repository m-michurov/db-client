package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Coach

class CoachesBySportDataSource : QueryDataSource<Coach>(
    """
        select
            "First_Name",
            "Last_Name",
            "Patronymic"
        from
            "Coach"
        where
            "Sport_Id" = :sport_id
    """.trimIndent(),
    parse = {
        var columnIndex= 1
        Coach(
            0,
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex),
            null
        )
    })
