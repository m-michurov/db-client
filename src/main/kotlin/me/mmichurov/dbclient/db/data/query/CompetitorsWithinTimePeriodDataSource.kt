package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.*

class CompetitorsWithinTimePeriodDataSource : QueryDataSource<ClubMembersCount>(
    """
        with "Participants_Count" as (
          select
            "Sport_Club"."Id",
            count(distinct "Sportsmen_Clubs"."Sportsman_Id") as "Sportsmen_Count"
          from
            "Sport_Club"
            join "Sportsmen_Clubs" on "Sport_Club"."Id" = "Sportsmen_Clubs"."Club_Id"
            join "Competitors" on "Competitors"."Sportsman_Id" = "Sportsmen_Clubs"."Sportsman_Id"
            join "Competition" on "Competition"."Id" = "Competitors"."Competition_Id"
          where
            "Competition"."Date" between :start_date and :end_date
          group by
            "Sport_Club"."Id")
        
        select
          "Name",
          "Sport",
          coalesce("Sportsmen_Count", 0) as "Sportsmen_Count"
        from
          "Sport_Club_View"
          left join "Participants_Count" on "Participants_Count"."Id" = "Club_Id"
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        ClubMembersCount(
            SportClub(
                0,
                getString(columnIndex++),
                Sport(
                    0,
                    getString(columnIndex++)
                )
            ),
            getInt(columnIndex)
        )
    }
)
