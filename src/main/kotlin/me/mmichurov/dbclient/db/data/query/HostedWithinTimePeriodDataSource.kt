package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.CompetitionHostDate
import me.mmichurov.dbclient.db.model.SportsFacility

class HostedWithinTimePeriodDataSource : QueryDataSource<CompetitionHostDate>(
    """
        select
          "Sports_Facility"."Address",
          "Sports_Facility"."Name",
          "Competition"."Date"
        from
          "Sports_Facility"
          join "Competition" on "Sports_Facility"."Id" = "Competition"."Sports_Facility_Id"
        where
          "Competition"."Date" between :start_date and :end_date
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        CompetitionHostDate(
            SportsFacility(
                0,
                getString(columnIndex++),
                getString(columnIndex++)
            ),
            getDate(columnIndex).toLocalDate()
        )
    }
)
