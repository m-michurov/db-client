package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.*
import java.time.LocalDate

class SportsmenByCoachDataSource : QueryDataSource<SportsmanRank>(
    mapOf(
        DEFAULT_QUERY to
                """
                    select
                      "Rank",
                      "Sportsman"."First_Name",
                      "Sportsman"."Last_Name"
                    from
                      "Sportsman"
                      join "Coaching" on "Sportsman"."Id" = "Coaching"."Sportsman_Id"
                      join "Coach" on "Coach"."Id" = "Coaching"."Coach_Id"
                      left join (
                        select "Sportsman_Id" as SID, "Sport_Id" as SP_ID, "Rank" from "Sportsmen_Ranks_View"
                      ) on (SID = "Sportsman"."Id" and "Coach"."Sport_Id" = SP_ID)
                    where
                      "Coach"."Id" = :coach_id
                """.trimIndent(),
        "rank" to
                """
                    select
                      "Rank"."Name",
                      "Sportsman"."First_Name",
                      "Sportsman"."Last_Name"
                    from
                      "Sportsman"
                      join "Coaching" on "Sportsman"."Id" = "Coaching"."Sportsman_Id"
                      join "Coach" on "Coach"."Id" = "Coaching"."Coach_Id"
                      join "Sportsmen_Sports" on (
                        "Sportsman"."Id" = "Sportsmen_Sports"."Sportsman_Id"
                        and "Coach"."Sport_Id" = "Sportsmen_Sports"."Sport_Id"
                      )
                      join "Rank" on "Rank"."Id" = "Sportsmen_Sports"."Sportsman_Rank_Id"
                    where
                      "Order_No" >= (
                        select
                          "Order_No"
                        from
                          "Rank"
                        where
                          "Id" = :rank_id
                      )
                      and "Coach_Id" = :coach_id
                """.trimIndent()
    ),
    parse = {
        var columnIndex = 1
        SportsmanRank(
            getString(columnIndex++)?.let { Rank(0, it) },
            Sport(),
            Sportsman(
                0,
                getString(columnIndex++),
                getString(columnIndex),
                "",
                LocalDate.EPOCH
            )
        )
    }
)