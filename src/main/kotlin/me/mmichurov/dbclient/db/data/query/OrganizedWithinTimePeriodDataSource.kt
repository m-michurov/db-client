package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.CompetitionOrganizer
import me.mmichurov.dbclient.db.model.OrganizedCompetitionsCount

class OrganizedWithinTimePeriodDataSource : QueryDataSource<OrganizedCompetitionsCount>(
    """
        with "Competitions_Count" as (
          select
            "Competition_Organizer"."Id" as "Organizer_Id",
            count(distinct "Competition"."Id") as "Competitions_Count"
          from
            "Competition_Organizer"
            join "Competition" on "Competition"."Organizer_Id" = "Competition_Organizer"."Id"
          where
            "Competition"."Date" between :start_date and :end_date
          group by
            "Competition_Organizer"."Id")
        
        select
          "Competition_Organizer"."Organization_Name",
          coalesce("Competitions_Count", 0) as "Competitions_Count"
        from
          "Competition_Organizer"
          left join "Competitions_Count" on "Competitions_Count"."Organizer_Id" = "Competition_Organizer"."Id"
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        OrganizedCompetitionsCount(
            CompetitionOrganizer(
                0,
                getString(columnIndex++)
            ),
            getInt(columnIndex)
        )
    }
)
