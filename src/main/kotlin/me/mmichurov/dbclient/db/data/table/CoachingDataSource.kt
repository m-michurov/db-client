package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.model.Coaching
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.Sportsman

class CoachingDataSource : TableDataSource<Coaching>(Coaching.VIEW, parse = {
    var columnIndex = 1
    Coaching(
        Coach(
            getInt(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            Sport(
                getInt(columnIndex++),
                getString(columnIndex++)
            )
        ),
        Sportsman(
            getInt(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getDate(columnIndex).toLocalDate()
        )
    )
})