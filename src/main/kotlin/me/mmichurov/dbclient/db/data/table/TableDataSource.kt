package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.data.DataSource
import tornadofx.invalidate
import tornadofx.runLater
import java.sql.ResultSet
import java.sql.SQLException

abstract class TableDataSource<T : DataAccessObject>(
    private val table: String,
    private val parse: ResultSet.() -> T
) : DataSource<T>() {

    private fun fetch(): List<T> = db.fetch(table, parse)

    /**
     * @throws SQLException
     */
    fun delete(item: T) = db.run {
        delete(item)
        items.remove(item)
    }

    /**
     * @throws SQLException
     */
    fun insert(item: T) = db.run {
        create(item)
        items.add(item)
    }

    /**
     * @throws SQLException
     */
    fun update(item: T) = db.run {
        update(item)
        items.invalidate()
    }

    /**
     * @throws SQLException
     */
    fun reload() = fetch().let {
        runLater { items.setAll(it) }
    }
}