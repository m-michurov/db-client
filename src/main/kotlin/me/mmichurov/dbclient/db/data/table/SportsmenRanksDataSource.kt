package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Rank
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.Sportsman
import me.mmichurov.dbclient.db.model.SportsmanRank

class SportsmenRanksDataSource : TableDataSource<SportsmanRank>(SportsmanRank.VIEW, parse = {
    var columnIndex = 1
    SportsmanRank(
        Rank(
            getInt(columnIndex++),
            getString(columnIndex++),
        ),
        Sport(
            getInt(columnIndex++),
            getString(columnIndex++),
        ),
        Sportsman(
            getInt(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getDate(columnIndex).toLocalDate()
        )
    )
})
