package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportClub

class SportClubDataSource : TableDataSource<SportClub>(SportClub.VIEW, parse = {
    var columnIndex = 1
    SportClub(
        getInt(columnIndex++),
        getString(columnIndex++),
        Sport(
            getInt(columnIndex++),
            getString(columnIndex)
        )
    )
})