package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Sport

class SportsDataSource : TableDataSource<Sport>(Sport.TABLE, parse = {
    var columnIndex = 1
    Sport(
        getInt(columnIndex++),
        getString(columnIndex))
})