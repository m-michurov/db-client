package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.CompetitionOrganizer

class CompetitionOrganizerDataSource :
    TableDataSource<CompetitionOrganizer>(CompetitionOrganizer.TABLE, parse = {
        var columnIndex = 1
        CompetitionOrganizer(
            getInt(columnIndex++),
            getString(columnIndex)
        )
    })