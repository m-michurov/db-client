package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Stadium

class StadiumDataSource : TableDataSource<Stadium>(Stadium.VIEW, parse = {
    var columnIndex = 1
    Stadium(
        getInt(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        getInt(columnIndex)
    )
})