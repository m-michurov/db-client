package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Rank
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportsmanRank

class SportsmanRanksDataSource : QueryDataSource<SportsmanRank>(
    """
        select
            "Rank",
            "Sport"
        from
            "Sportsmen_Ranks_View"
        where
            "Sportsman_Id" = :sportsman_id
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        SportsmanRank(
            Rank(0, getString(columnIndex++)),
            Sport(0, getString(columnIndex)),
            null
        )
    }
)