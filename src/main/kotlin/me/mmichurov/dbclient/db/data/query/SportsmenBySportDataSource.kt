package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.*
import java.time.LocalDate

class SportsmenBySportDataSource : QueryDataSource<SportsmanRank>(
    mapOf(
        DEFAULT_QUERY to
                """
                    with "Ranked_Sportsmen" as (
                      select distinct
                        "Sportsman_Id" as SID,
                        "Sport_Id"
                      from
                        "Sportsmen_Sports"
                    ),
                    "Competition_Participants" as (
                      select
                        "Sportsman_Id" as SID,
                        "Sport_Id"
                      from
                        "Competitors"
                        join "Competition" on ("Competition_Id" = "Id")
                    ),
                    "Trained_Sportsmen" as (
                      select
                        "Sportsman_Id" as SID,
                        "Sport_Id"
                      from
                        "Coaching"
                        join "Coach" on ("Coach_Id" = "Id")
                    ),
                    "Club_Members" as (
                      select
                        "Sportsman_Id" as SID,
                        "Sport_Id" 
                      from
                        "Sportsmen_Clubs"
                        join "Sport_Club" on ("Club_Id" = "Id")
                    )
                    
                    select distinct
                      "Rank",
                      "Sportsman"."First_Name",
                      "Sportsman"."Last_Name"
                    from
                      "Sportsman"
                      join (
                        (select * from "Ranked_Sportsmen")
                        union (select * from "Competition_Participants")
                        union (select * from "Trained_Sportsmen")
                        union (select * from "Club_Members")
                      ) on (SID = "Sportsman"."Id")
                      left join (
                        select "Sportsman_Id", "Sport_Id" as SP_ID, "Rank" from "Sportsmen_Ranks_View"
                      ) on ("Sportsman_Id" = "Sportsman"."Id" and "Sport_Id" = SP_ID)
                    where
                      "Sport_Id" = :sport_id
                """.trimIndent(),
        "rank" to
                """
                    select
                      "Rank"."Name",
                      "Sportsman"."First_Name",
                      "Sportsman"."Last_Name"
                    from
                      "Sportsman"
                      join "Sportsmen_Sports" on "Sportsman"."Id" = "Sportsmen_Sports"."Sportsman_Id"
                      join "Rank" on "Rank"."Id" = "Sportsmen_Sports"."Sportsman_Rank_Id"
                    where
                      "Order_No" >= (select "Order_No" from "Rank" where "Rank"."Id" = :rank_id)
                      and "Sportsmen_Sports"."Sport_Id" = :sport_id
                """.trimIndent()
    ),
    parse = {
        var columnIndex = 1
        SportsmanRank(
            getString(columnIndex++)?.let { Rank(0, it) },
            Sport(),
            Sportsman(
                0,
                getString(columnIndex++),
                getString(columnIndex),
                "",
                LocalDate.EPOCH
            )
        )
    }
)