package me.mmichurov.dbclient.db.data

import me.mmichurov.dbclient.db.DBController
import me.mmichurov.dbclient.db.model.DataAccessObject
import tornadofx.Controller
import tornadofx.asObservable

abstract class DataSource<T : DataAccessObject> : Controller() {

    private val dbController: DBController by inject()
    protected val db
        get() = dbController.db!!
    val items = mutableListOf<T>().asObservable()
}