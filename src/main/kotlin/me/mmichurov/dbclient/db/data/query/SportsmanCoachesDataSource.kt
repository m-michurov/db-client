package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.model.Sport

class SportsmanCoachesDataSource : QueryDataSource<Coach>(
    """select
              "First_Name",
              "Last_Name",
              "Patronymic",
              "Sports"."Name"
            from
              "Coach"
              join "Coaching" on ("Coach_Id" = "Coach"."Id")
              join "Sports" on ("Sport_Id" = "Sports"."Id")
            where
              "Sportsman_Id" = :sportsman_id""",
    parse = {
        var columnIndex = 1
        Coach(
            0,
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            Sport(0, getString(columnIndex))
        )
    })
