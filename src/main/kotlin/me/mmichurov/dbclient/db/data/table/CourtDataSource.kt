package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Court

class CourtDataSource : TableDataSource<Court>(Court.VIEW, parse = {
    var columnIndex = 1
    Court(
        getInt(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex)
    )
})