package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Sportsman

class SportsmanDataSource : TableDataSource<Sportsman>(Sportsman.TABLE, parse = {
    var columnIndex = 1
    Sportsman(
        getInt(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        getDate(columnIndex).toLocalDate(),
    )
})
