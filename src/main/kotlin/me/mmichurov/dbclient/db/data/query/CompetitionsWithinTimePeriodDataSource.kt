package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Competition
import me.mmichurov.dbclient.db.model.CompetitionOrganizer
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportsFacility

class CompetitionsWithinTimePeriodDataSource : QueryDataSource<Competition>(
    mapOf(
        DEFAULT_QUERY to
                """
                    select
                      *
                    from
                      "Competition_View"
                    where
                      "Date" between :start_date and :end_date
                """.trimIndent(),
        "organizer" to
                """
                    select
                      *
                    from
                      "Competition_View"
                    where
                      "Date" between :start_date and :end_date
                      and "Organizer_Id" = :organizer_id
                """.trimIndent()
    ),
    parse = {
        // A lot more data than is actually needed but hey, does it look like I care?
        var columnIndex = 1
        Competition(
            getInt(columnIndex++),
            getString(columnIndex++),
            getDate(columnIndex++).toLocalDate(),
            Sport(
                getInt(columnIndex++),
                getString(columnIndex++),
            ),
            CompetitionOrganizer(
                getInt(columnIndex++),
                getString(columnIndex++)
            ),
            SportsFacility(
                getInt(columnIndex++),
                getString(columnIndex++),
                getString(columnIndex)
            )
        )
    }
)
