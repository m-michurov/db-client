package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.SportsHall

class SportsHallDataSource : TableDataSource<SportsHall>(SportsHall.VIEW, parse = {
    var columnIndex = 1
    SportsHall(
        getInt(columnIndex++),
        getString(columnIndex++),
        getString(columnIndex++),
        getInt(columnIndex)
    )
})