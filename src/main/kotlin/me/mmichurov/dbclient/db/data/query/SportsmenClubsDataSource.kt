package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportClub

class SportsmenClubsDataSource : QueryDataSource<SportClub>(
    """
        select
            "Name",
            "Sport"
        from
            "Sport_Club_View"
            join "Sportsmen_Clubs" using ("Club_Id")
        where
            "Sportsman_Id" = :sportsman_id
    """.trimIndent(),
    parse = {
        SportClub(
            0,
            getString(1),
            Sport(
                0,
                getString(2)
            )
        )
    }
)