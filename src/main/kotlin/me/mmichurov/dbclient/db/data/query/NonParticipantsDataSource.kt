package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Sportsman

class NonParticipantsDataSource : QueryDataSource<Sportsman>(
    """
        with "Participants" as (
          select distinct
            "Sportsman"."Id"
          from
            "Sportsman"
            join "Competitors" on "Competitors"."Sportsman_Id" = "Sportsman"."Id"
            join "Competition" on "Competition"."Id" = "Competitors"."Competition_Id"
          where
            "Competition"."Date" between :start_date and :end_date
          group by
            "Sportsman"."Id"
        )
        
        select
          "First_Name",
          "Last_Name",
          "Patronymic",
          "Date_of_Birth"
        from
          "Sportsman"
        where
          "Id" not in (select * from "Participants")
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        Sportsman(
            0,
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getDate(columnIndex).toLocalDate()
        )
    })