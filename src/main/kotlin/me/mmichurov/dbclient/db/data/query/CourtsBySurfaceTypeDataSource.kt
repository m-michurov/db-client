package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Court

class CourtsBySurfaceTypeDataSource : QueryDataSource<Court>(
    """
        select
            "Name",
            "Address"
        from
            "Court_View"
        where
            "Surface_Type" = :surface_type
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        Court(
            0,
            getString(columnIndex++),
            getString(columnIndex),
            ""
        )
    }
)