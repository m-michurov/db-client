package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.SportsHall

class SportHallsByAreaDataSource : QueryDataSource<SportsHall>(
    """
        select
            "Name",
            "Address",
            "Area"
        from
            "Sports_Hall_View"
        where
            "Area" >= :area
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        SportsHall(
            0,
            getString(columnIndex++),
            getString(columnIndex++),
            getInt(columnIndex)
        )
    }
)