package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.*
import java.time.LocalDate

class CompetitorsDataSource : TableDataSource<Competitor>(Competitor.VIEW, parse = {
    var columnIndex = 1
    Competitor(
        Competition(
            getInt(columnIndex++),
            getString(columnIndex++),
            LocalDate.EPOCH,
            null,
            null,
            null
        ),
        Sportsman(
            getInt(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getDate(columnIndex++).toLocalDate()
        ),
        getString(columnIndex)
    )
})