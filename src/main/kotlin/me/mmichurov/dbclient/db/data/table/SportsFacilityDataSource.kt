package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.SportsFacility

class SportsFacilityDataSource : TableDataSource<SportsFacility>(SportsFacility.TABLE, parse = {
    var columnIndex = 1
    SportsFacility(
        getInt(columnIndex++),
        getString(1 + columnIndex++),
        getString(1 + columnIndex)
    )
})