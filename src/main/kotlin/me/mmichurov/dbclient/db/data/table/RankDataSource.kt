package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Rank

class RankDataSource : TableDataSource<Rank>(Rank.TABLE, parse = {
    var columnIndex = 1
    Rank(
        getInt(columnIndex++),
        getString(columnIndex)
    )
})