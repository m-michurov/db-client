package me.mmichurov.dbclient.db.data.table

import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportClub
import me.mmichurov.dbclient.db.model.Sportsman
import me.mmichurov.dbclient.db.model.SportsmanClub

class SportsmenClubsDataSource : TableDataSource<SportsmanClub>(SportsmanClub.VIEW, parse = {
    var columnIndex = 1
    SportsmanClub(
        SportClub(
            getInt(columnIndex++),
            getString(columnIndex++),
            Sport(
                getInt(columnIndex++),
                getString(columnIndex++)
            )
        ),
        Sportsman(
            getInt(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getString(columnIndex++),
            getDate(columnIndex).toLocalDate()
        )
    )
})