package me.mmichurov.dbclient.db.data.query

import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.Sportsman
import me.mmichurov.dbclient.db.model.SportsmanRank
import java.time.LocalDate

class MultiSportSportsmenDataSource : QueryDataSource<SportsmanRank>(
    """
        with "Ranked_Sportsmen" as (
          select
            distinct "Sportsman_Id",
            "Sport_Id"
          from
            "Sportsmen_Sports"
        ),
        "Competition_Participants" as (
          select
            "Sportsman_Id",
            "Sport_Id"
          from
            "Competitors"
            join "Competition" on ("Competition_Id" = "Id")
        ),
        "Trained_Sportsmen" as (
          select
            "Sportsman_Id",
            "Sport_Id"
          from
            "Coaching"
            join "Coach" on ("Coach_Id" = "Id")
        ),
        "Club_Members" as (
          select
            "Sportsman_Id",
            "Sport_Id"
          from
            "Sportsmen_Clubs"
            join "Sport_Club" on ("Club_Id" = "Id")
        ),
        "All_Sportsman_Sports" as (
          select
            distinct *
          from
          (select * from "Ranked_Sportsmen")
          union
          (select * from "Competition_Participants")
          union
          (select * from "Trained_Sportsmen")
          union
          (select * from "Club_Members")
        ),
        "Sports_Count" as (
          select
            "Sportsman_Id",
            count("Sport_Id") as "Count"
          from
            "All_Sportsman_Sports"
          group by
            "Sportsman_Id"
        ),
        "Multi_Sports" as (
          select
            distinct "Sportsman_Id"
          from
            "Sports_Count"
          where
            "Count" > 1
        )
        select
          "Sports"."Name",
          "First_Name",
          "Last_Name"
        from
          "Sportsman"
          join "Multi_Sports" on ("Multi_Sports"."Sportsman_Id" = "Sportsman"."Id")
          join "All_Sportsman_Sports" on (
            "All_Sportsman_Sports"."Sportsman_Id" = "Sportsman"."Id"
          )
          join "Sports" on ("Sport_Id" = "Sports"."Id")
        order by
          "Sportsman"."Id"
    """.trimIndent(),
    parse = {
        var columnIndex = 1
        SportsmanRank(
            null,
            Sport(0, getString(columnIndex++)),
            Sportsman(
                0,
                getString(columnIndex++),
                getString(columnIndex),
                null,
                LocalDate.EPOCH
            )
        )
    }
)