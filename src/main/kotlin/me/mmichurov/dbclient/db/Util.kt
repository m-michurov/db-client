package me.mmichurov.dbclient.db

infix fun <T, V> T.eq(v: V) = this to v
