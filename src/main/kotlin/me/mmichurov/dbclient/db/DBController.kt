package me.mmichurov.dbclient.db

import tornadofx.Controller
import java.sql.SQLException

class DBController : Controller() {

    var db: Database? = null

    /**
     * @throws SQLException
     */
    @Synchronized
    fun connect(
        username: String,
        password: String,
        host: String,
        port: Int,
        service: String = ""
    ) {
        db = Database.connect(username, password, host, port, service)
    }

    /**
     * @throws SQLException
     */
    fun disconnect() = db?.disconnect()
}