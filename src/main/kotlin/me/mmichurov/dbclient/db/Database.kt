package me.mmichurov.dbclient.db

import me.mmichurov.dbclient.db.model.DataAccessObject
import java.sql.*
import java.time.LocalDate
import java.util.*
import java.util.logging.Logger
import kotlin.collections.ArrayList

class Database private constructor(
    private val connection: Connection
) {

    private val prepared = mutableSetOf<PreparedStatement>()

    fun disconnect() {
        logger.info { "Disconnecting" }
        prepared.forEach {
            it.runCatching { close() }
        }
        connection.close()
        logger.info { "Disconnected" }
    }

    /**
     * @throws SQLException
     */
    fun prepare(query: String): PreparedStatement = connection.prepareStatement(query)

    /**
     * @throws SQLException
     */
    fun <T> fetch(tableOrView: String, parse: ResultSet.() -> T) =
        execute("select * from $tableOrView", parse)

    /**
     * @throws SQLException
     */
    fun <T> fetch(
        query: PreparedStatement,
        parameters: PreparedStatement.() -> Unit,
        parse: ResultSet.() -> T
    ) = execute(query, parameters, parse)

    /**
     * @throws SQLException
     */
    private fun update(statement: String): Int = execute(statement)

    /**
     * @throws SQLException
     */
    fun update(
        table: String,
        where: Pair<String, Any>,
        set: Array<Pair<String, Any?>>
    ): Int = update(
        "update $table set ${set.joinToString(",") { "${it.first}=${wrap(it.second)}" }} " +
                "where ${where.first}=${wrap(where.second)}"
    )

    /**
     * @throws SQLException
     */
    fun update(
        table: String,
        where: Array<Pair<String, Any?>>,
        set: Array<Pair<String, Any?>>
    ): Int = update(
        "update $table set ${set.joinToString(",") { "${it.first}=${wrap(it.second)}" }} " +
                "where ${where.joinToString(" and ") { "${it.first}=${wrap(it.second)}" }}"
    )

    /**
     * @throws SQLException
     */
    fun delete(
        table: String,
        where: Pair<String, Any>
    ) {
        execute("delete from $table where ${where.first}=${wrap(where.second)}")
    }

    /**
     * @throws SQLException
     */
    fun delete(
        table: String,
        where: Array<Pair<String, Any?>>
    ) {
        execute("delete from $table where ${
            where.joinToString(" and ") { "${it.first}=${wrap(it.second)}" }
        }")
    }

    /**
     * @throws SQLException
     */
    fun insert(
        table: String,
        vararg values: Any?
    ) {
        execute("insert into $table values (${values.joinToString(",") { wrap(it) }})")
    }

    /**
     * @throws SQLException
     */
    fun call(function: String, vararg args: Any?) {
        execute("begin $function(${args.joinToString(",") { wrap(it) }}); end;")
    }

    /**
     * @throws SQLException
     */
    fun <T : DataAccessObject> create(entry: T) = entry.create(db = this)

    /**
     * @throws SQLException
     */
    fun <T : DataAccessObject> update(entry: T) = entry.update(db = this)

    /**
     * @throws SQLException
     */
    fun <T : DataAccessObject> delete(entry: T) = entry.delete(db = this)

    /**
     * @throws SQLException
     */
    private fun <T> execute(query: String, parse: ResultSet.() -> T): List<T> {
        logger.info { "Fetching:\n$query" }
        return connection.prepareStatement(query).use {
            val row = it.executeQuery()
            val result = ArrayList<T>()
            while (row.next()) {
                result.add(row.parse())
            }
            result
        }
    }

    /**
     * @throws SQLException
     */
    private fun <T> execute(
        query: PreparedStatement,
        setParameters: PreparedStatement.() -> Unit,
        parse: ResultSet.() -> T
    ): List<T> {
        logger.info { "Fetching prepared:\n$query" }
        query.clearParameters()
        query.setParameters()
        return query.executeQuery().use { row ->
            val result = ArrayList<T>()
            while (row.next()) {
                result.add(row.parse())
            }
            result
        }
    }

    /**
     * @throws SQLException
     */
    private fun execute(statement: String): Int {
        logger.info { "Executing:\n$statement" }
        return connection.prepareStatement(statement).use {
            it.executeUpdate()
        }
    }

    companion object {

        private const val TIMEZONE = "GMT+7"
        private val LOCALE = Locale.ENGLISH

        val logger: Logger = Logger.getLogger(Database::class.java.simpleName)

        init {
            TimeZone.setDefault(TimeZone.getTimeZone(TIMEZONE))
            Locale.setDefault(LOCALE)
        }

        /**
         * @throws SQLException
         */
        fun connect(
            user: String,
            password: String,
            host: String,
            port: Int,
            service: String = ""
        ): Database {
            val url = "jdbc:oracle:thin:@$host:$port:$service"
            logger.info { "Connecting:\nTo $url as $user" }
            val connection = DriverManager.getConnection(url, user, password)
            return Database(connection)
        }

        private val String.sq
            get() = "'$this'"

        private fun wrap(it: Any?): String {
            return when (it) {
                is String -> it.trim().sq
                is LocalDate -> "to_date('${it.dayOfMonth}-${it.monthValue}-${it.year}', 'dd-mm-yyyy')"
                else -> "$it"
            }
        }
    }
}
