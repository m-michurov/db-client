package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class SportClub(
    id: Int,
    name: String,
    sport: Sport?
) : DataAccessObject {

    var id: Int by property(id)
    var name: String by property(name)
    var sport: Sport? by property(sport)

    constructor() : this(0, "", null)

    override fun create(db: Database) = db.insert(TABLE, id, sport?.id, name)

    override fun update(db: Database) = db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "Name".q to name,
            "Sport_Id".q to sport?.id
        )
    )

    override fun delete(db: Database) = db.delete(TABLE, where = "Id".q eq id)

    override fun toString() = "$name (${sport ?: "Ошибка"})"

    companion object {

        val VIEW = "Sport_Club_View".q
        val TABLE = "Sport_Club".q
    }
}