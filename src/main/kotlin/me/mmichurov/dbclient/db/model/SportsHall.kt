package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class SportsHall(
    id: Int,
    address: String,
    name: String,
    area: Int
) : SportsFacility(id, address, name) {

    var area: Int by property(area)

    constructor() : this(0, "", "", 0)

    override fun create(db: Database) = db.call(CREATE_FUNCTION, address, name, area)

    override fun update(db: Database) = db.update(
        SportsFacility.TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "Address".q to address,
            "Name".q to name
        )
    ) + db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf("Area".q to area)
    )

    override fun delete(db: Database) = db.call(DELETE_FUNCTION, id)

    companion object {

        val VIEW = "Sports_Hall_View".q

        private val TABLE = "Sports_Hall".q

        private val CREATE_FUNCTION = "Create_Sports_Hall".q
        private val DELETE_FUNCTION = "Delete_Sports_Facility".q
    }
}