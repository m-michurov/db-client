package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class SportsmanRank(
    rank: Rank?,
    sport: Sport?,
    sportsman: Sportsman?
) : DataAccessObject {

    var rank: Rank? by property(rank)
    var sport: Sport? by property(sport)
    var sportsman: Sportsman? by property(sportsman)

    constructor() : this(null, null, null)

    override fun create(db: Database) = db.insert(TABLE, rank?.id, sport?.id, sportsman?.id)

    override fun update(db: Database): Nothing =
        throw NotImplementedError("Rows of table $TABLE should not be updated")

    override fun delete(db: Database) = db.delete(
        TABLE,
        where = arrayOf(
            "Sportsman_Id".q eq sportsman?.id,
            "Sport_Id".q eq sport?.id
        )
    )

    companion object {

        val VIEW = "Sportsmen_Ranks_View".q
        val TABLE = "Sportsmen_Sports".q
    }
}
