package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property
import java.time.LocalDate

class Sportsman(
    id: Int,
    firstName: String,
    lastName: String,
    patronymic: String?,
    dateOfBirth: LocalDate,
    club: SportClub? = null
) : DataAccessObject {

    var id: Int by property(id)
    var firstName: String by property(firstName)
    var lastName: String by property(lastName)
    var patronymic: String? by property(patronymic)
    var dateOfBirth: LocalDate by property(dateOfBirth)
    var club: SportClub? by property(club)

    constructor() : this(0, "", "", null, LocalDate.EPOCH)

    override fun create(db: Database) = db.call(CREATE_FUNCTION, firstName, lastName, patronymic, dateOfBirth, club?.id)

    override fun update(db: Database) = db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "First_Name".q to firstName,
            "Last_Name".q to lastName,
            "Patronymic".q to patronymic,
            "Date_of_Birth".q to dateOfBirth,
        )
    )

    override fun delete(db: Database) = db.delete(TABLE, where = "Id".q eq id)

    override fun toString() = "$firstName $lastName"

    companion object {

        val TABLE = "Sportsman".q
        private val CREATE_FUNCTION = "Create_Sportsman".q
    }
}