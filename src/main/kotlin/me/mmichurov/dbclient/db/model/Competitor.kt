package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class Competitor(
    competition: Competition?,
    sportsman: Sportsman?,
    place: String?
) : DataAccessObject {

    var competition: Competition? by property(competition)
    var sportsman: Sportsman? by property(sportsman)
    var place: String? by property(place)

    constructor() : this(null, null, null)

    override fun create(db: Database) = db.insert(TABLE, competition?.id, sportsman?.id, place)

    override fun update(db: Database) = db.update(
        TABLE,
        where = arrayOf(
            "Competition_Id".q eq competition?.id,
            "Sportsman_Id".q eq sportsman?.id
        ),
        set = arrayOf(
            "Place".q to place
        )
    )

    override fun delete(db: Database) = db.delete(
        TABLE,
        where = arrayOf(
            "Competition_Id".q eq competition?.id,
            "Sportsman_Id".q eq sportsman?.id
        )
    )

    companion object {

        val VIEW = "Competitors_View".q
        val TABLE = "Competitors".q

        val PLACES = listOf("1", "2", "3")
    }
}