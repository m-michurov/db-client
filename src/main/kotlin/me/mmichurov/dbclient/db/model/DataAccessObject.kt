package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database

interface DataAccessObject {

    fun create(db: Database)

    fun update(db: Database): Int

    fun delete(db: Database)
}
