package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property
import java.time.LocalDate

class Competition(
    id: Int,
    name: String,
    date: LocalDate,
    sport: Sport?,
    organizer: CompetitionOrganizer?,
    place: SportsFacility?
) : DataAccessObject {

    var id: Int by property(id)
    var name: String by property(name)
    var date: LocalDate by property(date)
    var sport: Sport? by property(sport)
    var organizer: CompetitionOrganizer? by property(organizer)
    var place: SportsFacility? by property(place)

    constructor() : this(0, "", LocalDate.EPOCH, null, null, null)

    override fun create(db: Database) = db.insert(TABLE, id, sport?.id, organizer?.id, place?.id, name, date)

    override fun update(db: Database) = db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "Sport_Id".q to sport?.id,
            "Organizer_Id".q to organizer?.id,
            "Sports_Facility_Id".q to place?.id,
            "Name".q to name,
            "Date".q to date
        )
    )

    override fun delete(db: Database) = db.delete(TABLE, where = "Id".q eq id)

    override fun toString() = name

    companion object {

        val VIEW = "Competition_View".q
        val TABLE = "Competition".q
    }
}