package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import tornadofx.property

class Rank(
    id: Int,
    name: String
) :DataAccessObject {

    var id: Int by property(id)
    var name: String by property(name)

    override fun create(db: Database): Nothing =
        throw NotImplementedError("Contents of table $TABLE should not be modified")

    override fun update(db: Database): Nothing =
        throw NotImplementedError("Contents of table $TABLE should not be modified")

    override fun delete(db: Database): Nothing =
        throw NotImplementedError("Contents of table $TABLE should not be modified")

    override fun toString() = name

    companion object {

        val TABLE = "Rank".q
    }
}