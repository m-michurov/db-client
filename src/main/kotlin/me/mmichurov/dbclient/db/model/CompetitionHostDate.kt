package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import tornadofx.property
import java.time.LocalDate

class CompetitionHostDate(
    facility: SportsFacility,
    date: LocalDate
) :DataAccessObject {

    var facility: SportsFacility by property(facility)
    var date: LocalDate by property(date)

    override fun create(db: Database): Nothing = throw NotImplementedError()

    override fun update(db: Database): Nothing = throw NotImplementedError()

    override fun delete(db: Database): Nothing = throw NotImplementedError()
}