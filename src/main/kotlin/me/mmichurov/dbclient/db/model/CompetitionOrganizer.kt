package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class CompetitionOrganizer(
    id: Int,
    organizationName: String
) : DataAccessObject {

    var id: Int by property(id)
    var organizationName: String by property(organizationName)

    constructor() : this(0, "")

    override fun create(db: Database) = db.insert(TABLE, id, organizationName)

    override fun update(db: Database) = db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf("Organization_Name".q to organizationName)
    )

    override fun delete(db: Database) = db.delete(TABLE, where = "Id".q eq id)

    override fun toString() = organizationName

    companion object {

        val TABLE = "Competition_Organizer".q
    }
}