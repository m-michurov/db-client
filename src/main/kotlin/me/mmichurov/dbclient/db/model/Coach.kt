package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class Coach(
    id: Int,
    firstName: String,
    lastName: String,
    patronymic: String?,
    sport: Sport?
) : DataAccessObject {

    var id: Int by property(id)
    var firstName: String by property(firstName)
    var lastName: String by property(lastName)
    var patronymic: String? by property(patronymic)
    var sport: Sport? by property(sport)

    constructor() : this(0, "", "", null, null)

    override fun create(db: Database) = db.insert(TABLE, id, sport?.id,  firstName, lastName, patronymic)

    override fun update(db: Database) = db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "Sport_Id".q to sport?.id,
            "First_Name".q to firstName,
            "Last_Name".q to lastName,
            "Patronymic".q to patronymic
        )
    )

    override fun delete(db: Database) = db.delete(TABLE, where = "Id".q eq id)

    override fun toString() = "$firstName $lastName (${sport?.name})"

    companion object {

        val VIEW = "Coach_View".q
        val TABLE = "Coach".q
    }
}