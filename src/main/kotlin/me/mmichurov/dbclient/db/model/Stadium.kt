package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class Stadium(
    id: Int,
    address: String,
    name: String,
    capacity: Int
) : SportsFacility(id, address, name) {

    var capacity: Int by property(capacity)

    constructor() : this(0, "", "", 0)

    override fun create(db: Database) = db.call(CREATE_FUNCTION, address, name, capacity)

    override fun update(db: Database) = db.update(
        SportsFacility.TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "Address".q to address,
            "Name".q to name
        )
    ) + db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf("Capacity".q to capacity)
    )

    override fun delete(db: Database) = db.call(DELETE_FUNCTION, id)

    companion object {

        val VIEW = "Stadium_View".q

        private val TABLE = "Stadium".q

        private val CREATE_FUNCTION = "Create_Stadium".q
        private val DELETE_FUNCTION = "Delete_Sports_Facility".q
    }
}