package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class Court(
    id: Int,
    address: String,
    name: String,
    surfaceType: String
) : SportsFacility(id, address, name) {

    var surfaceType: String by property(surfaceType)

    constructor() : this(0, "", "", "")

    override fun create(db: Database) = db.call(CREATE_FUNCTION, address, name, surfaceType)

    override fun update(db: Database) = db.update(
        SportsFacility.TABLE,
        where = "Id".q eq id,
        set = arrayOf(
            "Address".q to address,
            "Name".q to name
        )
    ) + db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf("Surface_Type".q to surfaceType)
    )

    override fun delete(db: Database) = db.call(DELETE_FUNCTION, id)

    companion object {

        val VIEW = "Court_View".q

        private val TABLE = "Court".q

        private val CREATE_FUNCTION = "Create_Court".q
        private val DELETE_FUNCTION = "Delete_Sports_Facility".q

        val SURFACE_TYPES = listOf("grass", "hard-court", "clay")
    }
}