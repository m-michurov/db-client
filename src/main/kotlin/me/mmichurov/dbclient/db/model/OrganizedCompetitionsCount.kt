package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import tornadofx.property

class OrganizedCompetitionsCount(
    organizer: CompetitionOrganizer,
    organized: Int
) :DataAccessObject {

    var organizer: CompetitionOrganizer by property(organizer)
    var organized: Int by property(organized)

    override fun create(db: Database): Nothing = throw NotImplementedError()

    override fun update(db: Database): Nothing = throw NotImplementedError()

    override fun delete(db: Database): Nothing = throw NotImplementedError()
}