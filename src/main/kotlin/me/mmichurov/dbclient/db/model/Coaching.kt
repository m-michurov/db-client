package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class Coaching(
    coach: Coach?,
    sportsman: Sportsman?
) : DataAccessObject {

    var coach: Coach? by property(coach)
    var sportsman: Sportsman? by property(sportsman)

    constructor() : this(null, null)

    override fun create(db: Database) = db.insert(TABLE, coach?.id, sportsman?.id)

    override fun update(db: Database): Nothing =
        throw NotImplementedError("Rows of table $TABLE should not be updated")

    override fun delete(db: Database) = db.delete(
        TABLE,
        where = arrayOf(
            "Coach_Id".q eq coach?.id,
            "Sportsman_Id".q eq sportsman?.id
        )
    )

    companion object {

        val VIEW = "Coaching_View".q
        val TABLE = "Coaching".q
    }
}