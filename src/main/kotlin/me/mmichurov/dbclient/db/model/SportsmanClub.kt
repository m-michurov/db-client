package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class SportsmanClub(
    club: SportClub?,
    sportsman: Sportsman?
) : DataAccessObject {

    var club: SportClub? by property(club)
    var sportsman: Sportsman? by property(sportsman)

    constructor() : this(null, null)

    override fun create(db: Database) = db.insert(TABLE, club?.id, sportsman?.id)

    override fun update(db: Database): Nothing =
        throw NotImplementedError("Rows of table $TABLE should not be updated")

    override fun delete(db: Database) = db.delete(
        TABLE,
        where = arrayOf(
            "Club_Id".q eq club?.id,
            "Sportsman_Id".q eq sportsman?.id
        ))

    companion object {

        val VIEW = "Sportsmen_Clubs_View".q
        val TABLE = "Sportsmen_Clubs".q
    }
}