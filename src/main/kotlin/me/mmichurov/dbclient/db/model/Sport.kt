package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import me.mmichurov.dbclient.db.eq
import tornadofx.property

class Sport(
    id: Int,
    name: String
) : DataAccessObject {

    constructor() : this(0, "")

    var id: Int by property(id)
    var name: String by property(name)

    override fun create(db: Database) = db.insert(TABLE, id, name)

    override fun update(db: Database): Int = db.update(
        TABLE,
        where = "Id".q eq id,
        set = arrayOf("Name".q to name)
    )

    override fun delete(db: Database) = db.delete(TABLE, where = "Id".q eq id)

    override fun toString() = name

    companion object {

        val TABLE = "Sports".q
    }
}