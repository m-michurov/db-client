package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import tornadofx.property

open class SportsFacility(
    id: Int,
    address: String,
    name: String,
) : DataAccessObject {

    var id: Int by property(id)
    var address: String by property(address)
    var name: String by property(name)

    companion object {

        val TABLE = "Sports_Facility".q
    }

    override fun create(db: Database): Unit = throw NotImplementedError()

    override fun update(db: Database): Int = throw NotImplementedError()

    override fun delete(db: Database): Unit = throw NotImplementedError()

    override fun toString() = "$name, $address"
}