package me.mmichurov.dbclient.db.model

import me.mmichurov.dbclient.db.Database
import tornadofx.property

class ClubMembersCount(
    club: SportClub,
    members: Int
) :DataAccessObject {

    var club: SportClub by property(club)
    var members: Int by property(members)

    override fun create(db: Database): Nothing = throw NotImplementedError()

    override fun update(db: Database): Nothing = throw NotImplementedError()

    override fun delete(db: Database): Nothing = throw NotImplementedError()
}