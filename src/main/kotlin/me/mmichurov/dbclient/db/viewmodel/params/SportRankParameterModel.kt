package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.model.Rank
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.viewmodel.params.SportRankParameterModel.SportRankParameter
import tornadofx.property
import java.sql.PreparedStatement

class SportRankParameterModel : QueryParametersModel<SportRankParameter>(SportRankParameter()) {

    val sport = bind(SportRankParameter::sport)
    val rank = bind(SportRankParameter::rank)

    override fun queryId() = when {
        null != rank.value -> "rank"
        else -> QueryDataSource.DEFAULT_QUERY
    }

    override fun setParameters(query: PreparedStatement) = with(query) {
        rank.value?.let {
            setInt(1, rank.value.id)
            setInt(2, sport.value.id)
        } ?: run {
            setInt(1, sport.value.id)
        }
    }

    class SportRankParameter {
        var sport: Sport? by property(null)
        var rank: Rank? by property(null)
    }
}