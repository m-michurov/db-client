package me.mmichurov.dbclient.db.viewmodel.params

import java.sql.PreparedStatement

class NoParametersModel : QueryParametersModel<Unit>(Unit) {

    override fun setParameters(query: PreparedStatement) = Unit
}