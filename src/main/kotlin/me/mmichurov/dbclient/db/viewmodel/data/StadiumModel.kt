package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Stadium

class StadiumModel(item: Stadium? = null) : TableEntryModel<Stadium>(item) {

    val address = bind(Stadium::address)
    val name = bind(Stadium::name)
    val capacity = bind(Stadium::capacity)

    override fun clear() {
        address.value = ""
        name.value = ""
        capacity.value = 0
    }
}