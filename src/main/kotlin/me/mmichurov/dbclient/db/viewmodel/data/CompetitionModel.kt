package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Competition
import java.time.LocalDate

class CompetitionModel(item: Competition? = null) : TableEntryModel<Competition>(item) {

    val name = bind(Competition::name)
    val date = bind(Competition::date)
    val sport = bind(Competition::sport)
    val organizer = bind(Competition::organizer)
    val place = bind(Competition::place)

    override fun clear() {
        name.value = ""
        date.value = LocalDate.EPOCH
        sport.value = null
        organizer.value = null
        place.value = null
    }
}