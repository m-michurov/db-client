package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Sportsman
import java.time.LocalDate

open class SportsmanModel(item: Sportsman? = null) : TableEntryModel<Sportsman>(item) {

    val firstName = bind(Sportsman::firstName)
    val lastName = bind(Sportsman::lastName)
    val patronymic = bind(Sportsman::patronymic)
    val dateOfBirth = bind(Sportsman::dateOfBirth)
    val club = bind(Sportsman::club)

    override fun clear() {
        firstName.value = ""
        lastName.value = ""
        patronymic.value = null
        dateOfBirth.value = LocalDate.EPOCH
        club.value = null
    }
}