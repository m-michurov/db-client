package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel.DateRangeParameter
import tornadofx.property
import java.sql.Date
import java.sql.PreparedStatement
import java.time.LocalDate

class DateRangeParameterModel :
    QueryParametersModel<DateRangeParameter>(DateRangeParameter()) {

    val startDate = bind(DateRangeParameter::startDate)
    val endDate = bind(DateRangeParameter::endDate)

    override fun setParameters(query: PreparedStatement) {
        with(query) {
            var parameterIndex = 1
            setDate(parameterIndex++, Date.valueOf(startDate.value))
            setDate(parameterIndex, Date.valueOf(endDate.value))
        }
    }

    class DateRangeParameter {

        var startDate: LocalDate? by property(null)
        var endDate: LocalDate? by property(null)
    }
}
