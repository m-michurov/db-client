package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.CompetitionOrganizer

class CompetitionOrganizerModel(item: CompetitionOrganizer? = null) : TableEntryModel<CompetitionOrganizer>(item) {

    val organizationName = bind(CompetitionOrganizer::organizationName)

    override fun clear() {
        organizationName.value = ""
    }
}