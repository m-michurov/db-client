package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.SportsmanClub

class SportsmanClubModel(item: SportsmanClub? = null) : TableEntryModel<SportsmanClub>(item) {

    val club = bind(SportsmanClub::club)
    val sportsman = bind(SportsmanClub::sportsman)

    override fun clear() {
        club.value = null
        sportsman.value = null
    }
}