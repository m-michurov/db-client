package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Coaching

class CoachingModel(item: Coaching? = null) : TableEntryModel<Coaching>(item) {

    val coach = bind(Coaching::coach)
    val sportsman = bind(Coaching::sportsman)

    override fun clear() {
        coach.value = null
        sportsman.value = null
    }
}