package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.model.Sportsman
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel.SportsmanParameter
import tornadofx.property
import java.sql.PreparedStatement

class SportsmanParameterModel(sportsman: Sportsman? = null) :
    QueryParametersModel<SportsmanParameter>(SportsmanParameter(sportsman)) {

    val sportsman = bind(SportsmanParameter::sportsman)

    override fun setParameters(query: PreparedStatement) {
        with(query) {
            setInt(1, sportsman.value.id)
        }
    }

    class SportsmanParameter(sportsman: Sportsman? = null) {
        var sportsman: Sportsman? by property(sportsman)
    }
}