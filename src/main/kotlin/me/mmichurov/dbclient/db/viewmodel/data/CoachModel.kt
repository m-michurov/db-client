package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Coach

class CoachModel(item: Coach? = null) : TableEntryModel<Coach>(item) {

    val sport = bind(Coach::sport)
    val firstName = bind(Coach::firstName)
    val lastName = bind(Coach::lastName)
    val patronymic = bind(Coach::patronymic)

    override fun clear() {
        firstName.value = ""
        lastName.value = ""
        patronymic.value = ""
        sport.value = null
    }
}