package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.SportsmanRank

class SportsmanRankModel(item: SportsmanRank? = null) : TableEntryModel<SportsmanRank>(item) {

    val rank = bind(SportsmanRank::rank)
    val sport = bind(SportsmanRank::sport)
    val sportsman = bind(SportsmanRank::sportsman)

    override fun clear() {
        rank.value = null
        sport.value = null
        sportsman.value = null
    }
}