package me.mmichurov.dbclient.db.viewmodel.data

import javafx.beans.property.Property
import me.mmichurov.dbclient.db.model.Competition
import me.mmichurov.dbclient.db.model.Competitor
import java.time.LocalDate

class CompetitorModel(item: Competitor? = null) : TableEntryModel<Competitor>(item) {

    val competition = bind(Competitor::competition)
    val sportsman = bind(Competitor::sportsman)
    val place = bind(Competitor::place)

    override fun clear() {
        competition.value = null
        sportsman.value = null
        place.value = null
    }
}