package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.model.CompetitionOrganizer
import tornadofx.property
import java.time.LocalDate
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeOrganizerParameterModel.DateRangeOrganizerParameter
import java.sql.Date
import java.sql.PreparedStatement

class DateRangeOrganizerParameterModel :
    QueryParametersModel<DateRangeOrganizerParameter>(DateRangeOrganizerParameter()) {

    val startDate = bind(DateRangeOrganizerParameter::startDate)
    val endDate = bind(DateRangeOrganizerParameter::endDate)
    val organizer = bind(DateRangeOrganizerParameter::organizer)

    override fun queryId() = when {
        null != organizer.value -> "organizer"
        else -> QueryDataSource.DEFAULT_QUERY
    }

    override fun setParameters(query: PreparedStatement) {
        with(query) {
            var parameterIndex = 1
            setDate(parameterIndex++, Date.valueOf(startDate.value))
            setDate(parameterIndex++, Date.valueOf(endDate.value))
            organizer.value?.let {
                setInt(parameterIndex, it.id)
            }
        }
    }

    class DateRangeOrganizerParameter {

        var startDate: LocalDate? by property(null)
        var endDate: LocalDate? by property(null)
        var organizer: CompetitionOrganizer? by property(null)
    }
}
