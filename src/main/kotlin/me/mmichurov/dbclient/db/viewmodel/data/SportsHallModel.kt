package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.SportsHall

class SportsHallModel(item: SportsHall? = null) : TableEntryModel<SportsHall>(item) {

    val address = bind(SportsHall::address)
    val name = bind(SportsHall::name)
    val area = bind(SportsHall::area)

    override fun clear() {
        address.value = ""
        name.value = ""
        area.value = 0
    }
}