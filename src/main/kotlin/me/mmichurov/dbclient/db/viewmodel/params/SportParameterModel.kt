package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.viewmodel.params.SportParameterModel.SportParameter
import tornadofx.property
import java.sql.PreparedStatement

class SportParameterModel : QueryParametersModel<SportParameter>(SportParameter()) {

    val sport = bind(SportParameter::sport)

    override fun setParameters(query: PreparedStatement) = with(query) {
        setInt(1, sport.value.id)
    }

    class SportParameter {
        var sport: Sport? by property(null)
    }
}