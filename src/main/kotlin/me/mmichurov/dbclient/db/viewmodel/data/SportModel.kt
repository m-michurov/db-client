package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Sport

class SportModel(item: Sport? = null) : TableEntryModel<Sport>(item) {

    val name = bind(Sport::name)

    override fun clear() {
        name.value = ""
    }
}