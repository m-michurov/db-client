package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.model.SportsFacility
import me.mmichurov.dbclient.db.viewmodel.params.FacilitySportParameterModel.FacilitySportParameter
import tornadofx.property
import java.sql.PreparedStatement

class FacilitySportParameterModel : QueryParametersModel<FacilitySportParameter>(FacilitySportParameter()) {

    val facility = bind(FacilitySportParameter::facility)
    val sport = bind(FacilitySportParameter::sport)

    override fun queryId() = when {
        null != sport.value -> "sport"
        else -> QueryDataSource.DEFAULT_QUERY
    }

    override fun setParameters(query: PreparedStatement) {
        with(query) {
            var parameterIndex = 1
            setInt(parameterIndex++, facility.value.id)
            sport.value?.let {
                setInt(parameterIndex, it.id)
            }
        }
    }

    class FacilitySportParameter {
        var facility: SportsFacility? by property(null)
        var sport: Sport? by property(null)
    }
}