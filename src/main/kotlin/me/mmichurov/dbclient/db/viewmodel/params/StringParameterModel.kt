package me.mmichurov.dbclient.db.viewmodel.params

import tornadofx.property
import me.mmichurov.dbclient.db.viewmodel.params.StringParameterModel.StringParameter
import java.sql.PreparedStatement

class StringParameterModel(string: String? = null) :
    QueryParametersModel<StringParameter>(StringParameter(string)) {

    val value = bind(StringParameter::value)

    override fun setParameters(query: PreparedStatement) = with(query) {
        setString(1, value.value)
    }

    class StringParameter(value: String?) {
        var value: String? by property(value)
    }
}