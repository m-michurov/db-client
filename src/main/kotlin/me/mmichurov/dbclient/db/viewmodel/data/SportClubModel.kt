package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.SportClub

class SportClubModel(item: SportClub? = null) : TableEntryModel<SportClub>(item) {

    val name = bind(SportClub::name)
    val sport = bind(SportClub::sport)

    override fun clear() {
        name.value = ""
        sport.value = null
    }
}