package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.Court

class CourtModel(item: Court? = null) : TableEntryModel<Court>(item) {

    val address = bind(Court::address)
    val name = bind(Court::name)
    val surfaceType = bind(Court::surfaceType)

    override fun clear() {
        address.value = ""
        name.value = ""
        surfaceType.value = ""
    }
}