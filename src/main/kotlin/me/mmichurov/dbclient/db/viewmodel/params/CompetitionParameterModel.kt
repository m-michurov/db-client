package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.model.Competition
import tornadofx.property
import java.sql.PreparedStatement

class CompetitionParameterModel(competition: Competition? = null) :
    QueryParametersModel<CompetitionParameterModel.CompetitionParameter>(CompetitionParameter(competition)) {

    val competition = bind(CompetitionParameter::competition)

    override fun setParameters(query: PreparedStatement) = with(query) {
        setInt(1, competition.value.id)
    }

    class CompetitionParameter(competition: Competition? = null) {
        var competition: Competition? by property(competition)
    }
}