package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import tornadofx.ItemViewModel
import java.sql.PreparedStatement

abstract class QueryParametersModel<T>(item: T) : ItemViewModel<T>(item) {

    abstract fun setParameters(query: PreparedStatement)

    open fun queryId(): String = QueryDataSource.DEFAULT_QUERY
}