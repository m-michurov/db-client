package me.mmichurov.dbclient.db.viewmodel.data

import me.mmichurov.dbclient.db.model.DataAccessObject
import tornadofx.ItemViewModel

abstract class TableEntryModel<T : DataAccessObject>(item: T?) : ItemViewModel<T>(item) {

    /**
     * Clear all mutable fields
     */
    abstract fun clear()
}