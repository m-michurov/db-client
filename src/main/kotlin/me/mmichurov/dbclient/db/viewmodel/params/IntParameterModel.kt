package me.mmichurov.dbclient.db.viewmodel.params

import tornadofx.property
import me.mmichurov.dbclient.db.viewmodel.params.IntParameterModel.IntParameter
import java.sql.PreparedStatement

class IntParameterModel(string: Int? = null) :
    QueryParametersModel<IntParameter>(IntParameter(string)) {

    val value = bind(IntParameter::value)

    override fun setParameters(query: PreparedStatement) = with(query) {
        setInt(1, value.value)
    }

    class IntParameter(value: Int?) {
        var value: Int? by property(value)
    }
}