package me.mmichurov.dbclient.db.viewmodel.params

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.model.Rank
import me.mmichurov.dbclient.db.viewmodel.params.CoachRankParameterModel.CoachRankParameter
import tornadofx.property
import java.sql.PreparedStatement

class CoachRankParameterModel : QueryParametersModel<CoachRankParameter>(CoachRankParameter()) {

    val coach = bind(CoachRankParameter::coach)
    val rank = bind(CoachRankParameter::rank)

    override fun queryId() = when {
        null != rank.value -> "rank"
        else -> QueryDataSource.DEFAULT_QUERY
    }

    override fun setParameters(query: PreparedStatement) = with(query) {
        rank.value?.let {
            setInt(1, rank.value.id)
            setInt(2, coach.value.id)
        } ?: run {
            setInt(1, coach.value.id)
        }
    }

    class CoachRankParameter {
        var coach: Coach? by property(null)
        var rank: Rank? by property(null)
    }
}