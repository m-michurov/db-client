package me.mmichurov.dbclient

import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage
import jfxtras.styles.jmetro.JMetro
import jfxtras.styles.jmetro.MDL2IconFont
import jfxtras.styles.jmetro.Style
import me.mmichurov.dbclient.db.DBController
import me.mmichurov.dbclient.view.LoginForm
import tornadofx.*


class Application : App(LoginForm::class) {

    override fun createPrimaryScene(view: UIComponent): Scene {
        val scene = Scene(view.root, WIDTH, HEIGHT)
        JMetro(Style.LIGHT).scene = scene
        return scene
    }

    override fun start(stage: Stage) {
        stage.icons += Image("/ee94.png")  // Should never be and will never be changed
        super.start(stage)
    }

    override fun stop() {
        try {
            find<DBController>().disconnect()
        } finally {
            super.stop()
        }
    }

    companion object {

        private const val WIDTH = 1000.0
        private const val HEIGHT = 700.0
    }
}

fun main(args: Array<String>) {
    launch<Application>(args)
}
