package me.mmichurov.dbclient.view

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import me.mmichurov.dbclient.db.DBController
import tornadofx.*

private const val ENABLE_AUTOLOGIN_FEATURE = false

private data class Credentials(
    var username: String,
    var password: String,
    var host: String,
    var port: String,
    var service: String
) {

    constructor() : this("", "", "", "", "")
}

private class CredentialsModel : ItemViewModel<Credentials>(Credentials()) {
    val username = bind { SimpleStringProperty(item?.username, KEY_USERNAME, config.string(KEY_USERNAME)) }
    val password = bind { SimpleStringProperty(item?.password, KEY_PASSWORD, config.string(KEY_PASSWORD)) }
    val host = bind { SimpleStringProperty(item?.host, KEY_HOST, config.string(KEY_HOST)) }
    val port = bind { SimpleStringProperty(item?.port, KEY_PORT, config.string(KEY_PORT)) }
    val service = bind { SimpleStringProperty(item?.service, KEY_SERVICE, config.string(KEY_SERVICE)) }

    val remember = SimpleBooleanProperty(config.boolean(KEY_REMEMBER) ?: false)
    val autologin = SimpleBooleanProperty(config.boolean(KEY_AUTOLOGIN) ?: false)
    val expanded = SimpleBooleanProperty(config.boolean(KEY_EXPANDED) ?: false)

    override fun onCommit() {
        if (remember.value) {
            with(config) {
                set(KEY_USERNAME to username.value)
                set(KEY_PASSWORD to password.value)
                set(KEY_HOST to host.value)
                set(KEY_PORT to port.value)
                set(KEY_SERVICE to service.value)
                save()
            }
        }
    }

    companion object {

        private const val KEY_USERNAME = "username"
        private const val KEY_PASSWORD = "password"
        private const val KEY_HOST = "host"
        private const val KEY_PORT = "port"
        private const val KEY_SERVICE = "service"

        const val KEY_REMEMBER = "remember"
        const val KEY_AUTOLOGIN = "autologin"
        const val KEY_EXPANDED = "expanded"
    }
}

class LoginForm : View("Информационная система спортивных организаций города: вход") {

    private val dbController: DBController by inject()

    private val model = CredentialsModel()

    private val loginStatus = TaskStatus()

    override val root = borderpane {
        top = form {
            spacing = 2.0
            fieldset("Учетная запись пользователя") {
                field("Имя пользователя").textfield(model.username).required()
                field("Пароль").passwordfield(model.password).required()
            }
            squeezebox {
                fold("База данных", expanded = model.expanded.value) {
                    form {
                        fieldset("Параметры подключния к базе данных") {
                            field("Адрес").textfield(model.host).required()
                            field("Порт").textfield(model.port) {
                                required()
                                positiveNumbersOnly()
                            }
                            field("Сервис").textfield(model.service)
                        }
                    }
                    expandedProperty().addListener { _, _, expanded ->
                        with(model.config) {
                            set(CredentialsModel.KEY_EXPANDED to expanded)
                            save()
                        }
                    }
                }
            }
            fieldset {
                spacing = 1.0
                checkbox("Запомнить", model.remember).action {
                    with(model.config) {
                        set(CredentialsModel.KEY_REMEMBER to model.remember.value)
                        save()
                    }
                }
                if (!ENABLE_AUTOLOGIN_FEATURE) return@fieldset
                checkbox("Выполнять вход автоматически", model.autologin) {
                    isDisable = true
                    tooltip("Не реализовано в достаточном объёме")
                    action {
                        with(model.config) {
                            set(CredentialsModel.KEY_AUTOLOGIN to model.autologin.value)
                            save()
                        }
                    }
                }
            }
            buttonbar {
                button("Войти") {
                    enableWhen(model.valid.and(loginStatus.running.not()))
                    action { login() }
                }
            }
        }
    }

    init {
        if (ENABLE_AUTOLOGIN_FEATURE && model.autologin.value) {
            login()
        }
    }

    private fun login() {
        model.commit {
            runAsync(loginStatus) {
                dbController.connect(
                    model.username.value,
                    model.password.value,
                    model.host.value,
                    Integer.parseInt(model.port.value),
                    model.service.value ?: ""
                )
            } success {
                replaceWith<Main>()
            } fail {
                errorPopup("Не удалось подключиться к базе данных", it)
            }
        }
    }
}