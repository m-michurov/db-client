package me.mmichurov.dbclient.view

import javafx.geometry.Pos
import me.mmichurov.dbclient.view.query.*
import me.mmichurov.dbclient.view.table.*
import tornadofx.*

class Main : View("Информационная система спортивных организаций города") {

    override val root = borderpane {
        center = tabpane {
            tab("Таблицы") {
                isClosable = false
                vbox {
                    spacing = 1.0

                    label("Сущности").alignment = Pos.CENTER
                    separator()

                    button("Виды спорта").action { replaceWith<SportsTable>() }
                    button("Спортсмены").action { replaceWith<SportsmanTable>() }
                    button("Тренеры").action { replaceWith<CoachTable>() }
                    button("Спортивные клубы").action { replaceWith<SportClubTable>() }
                    button("Стадионы").action { replaceWith<StadiumTable>() }
                    button("Спортивные залы").action { replaceWith<SportsHallTable>() }
                    button("Корты").action { replaceWith<CourtTable>() }
                    button("Организаторы соревнований").action { replaceWith<CompetitionOrganizerTable>() }
                    button("Соревнования").action { replaceWith<CompetitionTable>() }

                    separator()
                    label("Связи").alignment = Pos.CENTER
                    separator()

                    button("Члены спортивных клубов").action { replaceWith<SportsmenClubsTable>() }
                    button("Спортивные разряды спортсменов").action { replaceWith<SportsmenRanksTable>() }
                    button("Тренеры спортсменов").action { replaceWith<CoachingTable>() }
                    button("Участники соревнований").action { replaceWith<CompetitorsTable>() }
                    setMaxWidthForChildren()
                }
            }

            tab("Запросы") {
                isClosable = false
                vbox {
                    spacing = 1.0

                    label("Спортсмены").alignment = Pos.CENTER
                    separator()
                    button("Спортсмены, занимающиеся указанным видом спорта").action { replaceWith<SportsmenBySportQuery>() }
                    button("Спортсмены, тренирующиеся у указанного тренера").action { replaceWith<SportsmenByCoachQuery>() }
                    button("Спортсмены, занимающиеся более чем одним видом спорта").action { replaceWith<MultiSportSportsmenQuery>() }
                    button("Спортсмены, не участвовавше в соренованиях в течение указанного пероида времени").action { replaceWith<NonParticipantsQuery>() }

                    separator()
                    label("Тренеры").alignment = Pos.CENTER
                    separator()

                    button("Тренеры по определеному виду спорта").action { replaceWith<CoachesBySportQuery>() }
                    button("Тренеры указанного спортсмена").action { replaceWith<SportsmanCoachesQuery>() }

                    separator()
                    label("Спортивные сооружения").alignment = Pos.CENTER
                    separator()

                    button("Корты с указанным типом покрытия").action { replaceWith<CourtsBySurfaceTypeQuery>() }
                    button("Стадионы с указанной вместимостью").action { replaceWith<StadiumsByCapacityQuery>() }
                    button("Спортивные залы с указанной площадью").action { replaceWith<SportHallsByAreaQuery>() }

                    separator()
                    label("Соревнования").alignment = Pos.CENTER
                    separator()

                    button("Соревнования, проведенные в течение указанного периода времени").action { replaceWith<CompetitionsWithinTimePeriodQuery>() }
                    button("Призеры соревнования").action { replaceWith<PrizeWinnersQuery>() }
                    button("Соревнования, проведенные в указанном спортивном сооружении").action { replaceWith<CompetitionByFacilityQuery>() }
                    button("Число спортсменов, участвовавших в соревнованиях в течение указанного периода времени").action { replaceWith<CompetitorsWithinTimePeriodQuery>() }
                    button("Организаторы и число проведенных ими соревнований за указанный период времени").action { replaceWith<OrganizedWithinTimePeriodQuery>() }
                    button("Спортивные сооружения и даты проведения в них соревнований за указанный период времени").action { replaceWith<HostedWithinTimePeriodQuery>() }

                    setMaxWidthForChildren()
                }
            }
        }

        bottom = button("Выйти") {
            useMaxWidth = true
            action { close() }
        }
    }
}