package me.mmichurov.dbclient.view.query.modal

import me.mmichurov.dbclient.db.model.SportClub
import me.mmichurov.dbclient.db.data.query.SportsmenClubsDataSource
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel.SportsmanParameter
import tornadofx.column
import tornadofx.find

class SportsmanClubsModalQuery(
    name: String,
    model: SportsmanParameterModel
) : ModalQuery<
        SportClub,
        SportsmanParameter,
        SportsmanParameterModel,
        SportsmenClubsDataSource>(
    name,
    model,
    find(),
    columns = {
        column("Название", SportClub::name)
        column("Вид спорта", SportClub::sport)
    }
)