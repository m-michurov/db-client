package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.viewmodel.params.QueryParametersModel
import me.mmichurov.dbclient.view.errorPopup
import me.mmichurov.dbclient.view.setMaxWidthForChildren
import tornadofx.*
import kotlin.reflect.KClass

abstract class QueryParametersEditor<T, U : QueryParametersModel<T>>(
    dataSourceClass: KClass<out QueryDataSource<out DataAccessObject>>,
    private val model: U,
    fields: Fieldset.(U) -> Unit
) : Fragment("Параметры запроса") {

    private val dataSource = find(dataSourceClass)
    private val submitStatus = TaskStatus()

    override val root = form {
        fieldset("Параметры запроса") {
            fields(model)
        }
        buttonbar {
            button("Найти") {
                enableWhen(model.dirty and model.valid and !submitStatus.running)
                action { submit() }
            }
            setMaxWidthForChildren()
        }
    }

    init {
        model.validate(decorateErrors = true)
    }

    private fun submit() {
        runAsync(submitStatus) {
            model.commit {
                dataSource.query(model.queryId()) {
                    model.setParameters(it)
                }
            }
        } fail {
            errorPopup("Не удалось выполниь запрос", it)
        }
    }
}