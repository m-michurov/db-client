package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.CourtsBySurfaceTypeDataSource
import me.mmichurov.dbclient.db.model.Court
import me.mmichurov.dbclient.db.viewmodel.params.StringParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.StringParameterModel.StringParameter
import tornadofx.*

/* Query #1 */
class CourtsBySurfaceTypeQuery : Query<Court, StringParameter, StringParameterModel, CourtsBySurfaceTypeDataSource>(
    "Корты с указанным типом покрытия",
    CourtsBySurfaceTypeParametersEditor::class,
    CourtsBySurfaceTypeDataSource::class,
    columns = {
        column("Адрес", Court::address)
        column("Название", Court::name)
    }
) {

    class CourtsBySurfaceTypeParametersEditor : QueryParametersEditor<StringParameter, StringParameterModel>(
        CourtsBySurfaceTypeDataSource::class,
        StringParameterModel(),
        fields = { model ->
            field("Тип покрытия").combobox(model.value, values = Court.SURFACE_TYPES) {
                required()
            }
        }
    )
}