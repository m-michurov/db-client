package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.PrizeWinnersDataSource
import me.mmichurov.dbclient.db.data.table.CompetitionDataSource
import me.mmichurov.dbclient.db.model.Competitor
import me.mmichurov.dbclient.db.viewmodel.params.CompetitionParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.CompetitionParameterModel.CompetitionParameter
import tornadofx.*

/* Query #7 */
class PrizeWinnersQuery : Query<Competitor, CompetitionParameter, CompetitionParameterModel, PrizeWinnersDataSource>(
    "Призеры указанного соревнования",
    PrizeWinnersParametersEditor::class,
    PrizeWinnersDataSource::class,
    columns = {
        column("Спортсмен", Competitor::sportsman)
        column("Место", Competitor::place)
    }
) {

    class PrizeWinnersParametersEditor : QueryParametersEditor<CompetitionParameter, CompetitionParameterModel>(
        PrizeWinnersDataSource::class,
        CompetitionParameterModel(),
        fields = { model ->
            field("Соревнование").combobox(model.competition, values = find<CompetitionDataSource>().items) {
                required()
            }
        }
    ) {

        override fun onDock() = find<CompetitionDataSource>().reload()
    }
}