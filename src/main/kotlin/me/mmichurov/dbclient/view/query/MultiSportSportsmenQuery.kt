package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.MultiSportSportsmenDataSource
import me.mmichurov.dbclient.db.data.query.SportsmenByCoachDataSource
import me.mmichurov.dbclient.db.model.SportsmanRank
import me.mmichurov.dbclient.db.viewmodel.params.NoParametersModel
import tornadofx.column

/* Query #4 */
class MultiSportSportsmenQuery : Query<
        SportsmanRank,
        Unit,
        NoParametersModel,
        MultiSportSportsmenDataSource>(
    "Спортсмены, занимающиеся более чем одним видом спорта",
    MultiSportSportsmenParametersEditor::class,
    MultiSportSportsmenDataSource::class,
    columns = {
        column("Спортсмен", SportsmanRank::sportsman)
        column("Вид спорта", SportsmanRank::sport)
    },
    queryOnDock = true
) {

    class MultiSportSportsmenParametersEditor : EmptyEditor(SportsmenByCoachDataSource::class)
}