package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.SportsmenByCoachDataSource
import me.mmichurov.dbclient.db.data.table.CoachDataSource
import me.mmichurov.dbclient.db.data.table.RankDataSource
import me.mmichurov.dbclient.db.model.SportsmanRank
import me.mmichurov.dbclient.db.viewmodel.params.CoachRankParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.CoachRankParameterModel.CoachRankParameter
import tornadofx.*

/* Query #3 */
class SportsmenByCoachQuery : Query<
        SportsmanRank,
        CoachRankParameter,
        CoachRankParameterModel,
        SportsmenByCoachDataSource>(
    "Спортсмены, тренирующиеся у указанного тренера",
    SportsmenByCoachParametersEditor::class,
    SportsmenByCoachDataSource::class,
    columns = {
        column("Спортсмен", SportsmanRank::sportsman)
        column("Разряд", SportsmanRank::rank)
    }
) {

    class SportsmenByCoachParametersEditor : QueryParametersEditor<
            CoachRankParameter,
            CoachRankParameterModel>(
        SportsmenByCoachDataSource::class,
        CoachRankParameterModel(),
        fields = { model ->
            field("Тренер").combobox(model.coach, values = find<CoachDataSource>().items) {
                useMaxWidth = true
                required()
            }
            field("Разряд\nне ниже") {
                combobox(model.rank, values = find<RankDataSource>().items)
                button("✖") {
                    tooltip("Очистить")
                    action { model.rank.value = null }
                }
            }
        }
    ) {

        override fun onDock() {
            find<RankDataSource>().reload()
            find<CoachDataSource>().reload()
            super.onDock()
        }
    }
}