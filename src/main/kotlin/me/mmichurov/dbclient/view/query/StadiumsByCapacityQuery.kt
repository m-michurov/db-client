package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.StadiumsByCapacityDataSource
import me.mmichurov.dbclient.db.model.Stadium
import me.mmichurov.dbclient.db.viewmodel.params.IntParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.IntParameterModel.IntParameter
import me.mmichurov.dbclient.view.positiveNumbersOnly
import tornadofx.column
import tornadofx.field
import tornadofx.required
import tornadofx.textfield

/* Query #1 */
class StadiumsByCapacityQuery : Query<Stadium, IntParameter, IntParameterModel, StadiumsByCapacityDataSource>(
    "Стадионы с указанной вместимостью",
    StadiumsByCapacityParametersEditor::class,
    StadiumsByCapacityDataSource::class,
    columns = {
        column("Адрес", Stadium::address)
        column("Название", Stadium::name)
        column("Вместимость", Stadium::capacity)
    }
) {

    class StadiumsByCapacityParametersEditor : QueryParametersEditor<IntParameter, IntParameterModel>(
        StadiumsByCapacityDataSource::class,
        IntParameterModel(),
        fields = { model ->
            field("Вместимость не менее").textfield(model.value) {
                positiveNumbersOnly()
                required()
            }
        }
    )
}