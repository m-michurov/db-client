package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.OrganizedWithinTimePeriodDataSource
import me.mmichurov.dbclient.db.model.OrganizedCompetitionsCount
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel.DateRangeParameter
import tornadofx.*


/* Query #12 */
class OrganizedWithinTimePeriodQuery : Query<
        OrganizedCompetitionsCount,
        DateRangeParameter,
        DateRangeParameterModel,
        OrganizedWithinTimePeriodDataSource>(
    "Организаторы и число проведенных ими соревнований за указанный период времени",
    OrganizedWithinTimePeriodParametersEditor::class,
    OrganizedWithinTimePeriodDataSource::class,
    columns = {
        column("Организатор", OrganizedCompetitionsCount::organizer)
        column("Число проведенных соревнований", OrganizedCompetitionsCount::organized)
    }
) {

    class OrganizedWithinTimePeriodParametersEditor : QueryParametersEditor<
            DateRangeParameter,
            DateRangeParameterModel>(
        OrganizedWithinTimePeriodDataSource::class,
        DateRangeParameterModel(),
        fields = { model ->
            field("С").datepicker(model.startDate) {
                useMaxWidth = true
                required()
            }
            field("До").datepicker(model.endDate) {
                useMaxWidth = true
                required()
            }
        }
    )
}