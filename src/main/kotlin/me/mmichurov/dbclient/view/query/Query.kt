package me.mmichurov.dbclient.view.query

import javafx.scene.control.TableView
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.view.setMaxWidthForChildren
import me.mmichurov.dbclient.view.notFocusTraversable
import me.mmichurov.dbclient.db.viewmodel.params.QueryParametersModel
import me.mmichurov.dbclient.view.Main
import tornadofx.*
import kotlin.reflect.KClass

abstract class Query<E : DataAccessObject, P, M : QueryParametersModel<P>, C : QueryDataSource<E>>(
    name: String,
    editorClass: KClass<out QueryParametersEditor<P, M>>,
    dataSourceClass: KClass<C>,
    columns: TableView<E>.() -> Unit,
    private val queryOnDock: Boolean = false
) : View(name) {

    private val dataSource = find(dataSourceClass)
    private val editor = find(editorClass)

    override val root = borderpane {
        center = vbox {
            tableview(dataSource.items) {
                prefHeightProperty().bind(primaryStage.heightProperty())
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY

                columns()
            }
        }

        right = borderpane {
            top = vbox {
                add(editorClass)
                separator()
            }

            bottom = vbox {
                spacing = 1.0
                separator()
                button("Назад") {
                    action { replaceWith<Main>() }
                }
                setMaxWidthForChildren()
                notFocusTraversable()
            }
        }
    }

    override fun onDock() = if (queryOnDock) dataSource.query {} else Unit
}