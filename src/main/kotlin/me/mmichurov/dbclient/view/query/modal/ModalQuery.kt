package me.mmichurov.dbclient.view.query.modal

import javafx.scene.control.TableView
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.viewmodel.params.QueryParametersModel
import tornadofx.Fragment
import tornadofx.borderpane
import tornadofx.tableview
import tornadofx.vbox

abstract class ModalQuery<E : DataAccessObject, P, M : QueryParametersModel<P>, C : QueryDataSource<E>>(
    name: String,
    model: M,
    controller: C,
    columns: TableView<E>.() -> Unit
) : Fragment(name) {

    override val root = borderpane {
        center = vbox {
            tableview(controller.items) {
                prefWidthProperty().bind(primaryStage.widthProperty().multiply(0.75))
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY

                columns()
            }
        }
    }

    init {
        controller.query { model.setParameters(it) }
    }
}