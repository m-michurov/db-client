package me.mmichurov.dbclient.view.query.modal

import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.data.query.SportsmanCoachesDataSource
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel
import tornadofx.column
import tornadofx.find

class SportsmanCoachesModalQuery(
    name: String,
    model: SportsmanParameterModel
) : ModalQuery<
        Coach,
        SportsmanParameterModel.SportsmanParameter,
        SportsmanParameterModel,
        SportsmanCoachesDataSource>(
    name,
    model,
    find(),
    columns = {
        column("Имя", Coach::firstName)
        column("Фамилия", Coach::lastName)
        column("Отчество", Coach::patronymic)
        column("Вид спорта", Coach::sport)
    })