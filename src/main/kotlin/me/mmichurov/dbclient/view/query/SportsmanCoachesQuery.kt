package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.SportsmanCoachesDataSource
import me.mmichurov.dbclient.db.data.table.SportsmanDataSource
import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel.SportsmanParameter
import tornadofx.*

/* Query #5 */
class SportsmanCoachesQuery : Query<Coach, SportsmanParameter, SportsmanParameterModel, SportsmanCoachesDataSource>(
    "Тренеры указанного спортсмена",
    SportsmanCoachesParametersEditor::class,
    SportsmanCoachesDataSource::class,
    columns = {
        column("Имя", Coach::firstName)
        column("Фамилия", Coach::lastName)
        column("Отчество", Coach::patronymic)
        column("Вид спорта", Coach::sport)
    }
) {

    class SportsmanCoachesParametersEditor : QueryParametersEditor<SportsmanParameter, SportsmanParameterModel>(
        SportsmanCoachesDataSource::class,
        SportsmanParameterModel(),
        fields = { model ->
            field("Спортсмен").combobox(model.sportsman, values = find<SportsmanDataSource>().items) {
                required()
            }
        }
    ) {

        override fun onDock() = find<SportsmanDataSource>().reload()
    }
}

