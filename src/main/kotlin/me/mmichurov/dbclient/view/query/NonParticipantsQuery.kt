package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.NonParticipantsDataSource
import me.mmichurov.dbclient.db.model.Sportsman
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel.DateRangeParameter
import tornadofx.*

/* Query #11 */
class NonParticipantsQuery : Query<
        Sportsman,
        DateRangeParameter,
        DateRangeParameterModel,
        NonParticipantsDataSource>(
    "Спортсмены, не участвовавше в соренованиях в течение указанного пероида времени",
    NonParticipantsParametersEditor::class,
    NonParticipantsDataSource::class,
    columns = {
        column("Имя", Sportsman::firstName)
        column("Фамилия", Sportsman::lastName)
        column("Отчество", Sportsman::patronymic)
        column("Дата рождения", Sportsman::dateOfBirth)
    }
) {

    class NonParticipantsParametersEditor :
        QueryParametersEditor<DateRangeParameter, DateRangeParameterModel>(
            NonParticipantsDataSource::class,
            DateRangeParameterModel(),
            fields = { model ->
                field("С").datepicker(model.startDate) {
                    useMaxWidth = true
                    required()
                }
                field("До").datepicker(model.endDate) {
                    useMaxWidth = true
                    required()
                }
            }
    )
}