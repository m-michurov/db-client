package me.mmichurov.dbclient.view.query.modal

import me.mmichurov.dbclient.db.data.query.CompetitorsDataSource
import me.mmichurov.dbclient.db.model.Competitor
import me.mmichurov.dbclient.db.viewmodel.params.CompetitionParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.CompetitionParameterModel.CompetitionParameter
import tornadofx.column
import tornadofx.find

class CompetitionParticipantsModalQuery(
    name: String,
    model: CompetitionParameterModel
) : ModalQuery<
        Competitor,
        CompetitionParameter,
        CompetitionParameterModel,
        CompetitorsDataSource>(
    name,
    model,
    find(),
    columns = {
        column("Спортсмен", Competitor::sportsman)
        column("Место", Competitor::place)
    }
)