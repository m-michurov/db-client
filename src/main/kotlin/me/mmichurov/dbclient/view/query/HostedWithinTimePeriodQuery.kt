package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.HostedWithinTimePeriodDataSource
import me.mmichurov.dbclient.db.model.CompetitionHostDate
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel.DateRangeParameter
import tornadofx.*


/* Query #13 */
class HostedWithinTimePeriodQuery : Query<
        CompetitionHostDate,
        DateRangeParameter,
        DateRangeParameterModel,
        HostedWithinTimePeriodDataSource>(
    "Спортивные сооружения и даты проведения в них соревнований за указанный период времени",
    HostedWithinTimePeriodParametersEditor::class,
    HostedWithinTimePeriodDataSource::class,
    columns = {
        column("Спртивное сооружение", CompetitionHostDate::facility)
        column("Дата проведения соревнования", CompetitionHostDate::date)
    }
) {

    class HostedWithinTimePeriodParametersEditor : QueryParametersEditor<
            DateRangeParameter,
            DateRangeParameterModel>(
        HostedWithinTimePeriodDataSource::class,
        DateRangeParameterModel(),
        fields = { model ->
            field("С").datepicker(model.startDate) {
                useMaxWidth = true
                required()
            }
            field("До").datepicker(model.endDate) {
                useMaxWidth = true
                required()
            }
        }
    )
}