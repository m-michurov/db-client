package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.CompetitionsWithinTimePeriodDataSource
import me.mmichurov.dbclient.db.data.table.CompetitionOrganizerDataSource
import me.mmichurov.dbclient.db.model.Competition
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeOrganizerParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeOrganizerParameterModel.DateRangeOrganizerParameter
import tornadofx.*


/* Query #6 */
class CompetitionsWithinTimePeriodQuery : Query<
        Competition,
        DateRangeOrganizerParameter,
        DateRangeOrganizerParameterModel,
        CompetitionsWithinTimePeriodDataSource>(
    "Соревнования, проведенные в течение указанного периода времени",
    CompetitionsWithinTimePeriodParametersEditor::class,
    CompetitionsWithinTimePeriodDataSource::class,
    columns = {
        column("Название", Competition::name)
        column("Дата проведения", Competition::date)
        column("Вид спорта", Competition::sport)
        column("Организатор", Competition::organizer)
        column("Место проведения", Competition::place)
    }
) {

    class CompetitionsWithinTimePeriodParametersEditor : QueryParametersEditor<
            DateRangeOrganizerParameter,
            DateRangeOrganizerParameterModel>(
        CompetitionsWithinTimePeriodDataSource::class,
        DateRangeOrganizerParameterModel(),
        fields = { model ->
            field("С").datepicker(model.startDate) {
                useMaxWidth = true
                required()
            }
            field("До").datepicker(model.endDate) {
                useMaxWidth = true
                required()
            }
            field("Организатор") {
                combobox(model.organizer, values = find<CompetitionOrganizerDataSource>().items)
                button("✖") {
                    tooltip("Очистить")
                    action { model.organizer.value = null }
                }
            }
        }
    ) {

        override fun onDock() {
            find<CompetitionOrganizerDataSource>().reload()
            super.onDock()
        }
    }
}