package me.mmichurov.dbclient.view.query.modal

import me.mmichurov.dbclient.db.model.SportsmanRank
import me.mmichurov.dbclient.db.data.query.SportsmanRanksDataSource
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel.SportsmanParameter
import tornadofx.column
import tornadofx.find

class SportsmanRanksModalQuery(
    name: String,
    model: SportsmanParameterModel
) : ModalQuery<
        SportsmanRank,
        SportsmanParameter,
        SportsmanParameterModel,
        SportsmanRanksDataSource>(
    name,
    model,
    find(),
    columns = {
        column("Разряд", SportsmanRank::rank)
        column("Вид спорта", SportsmanRank::sport)
    })