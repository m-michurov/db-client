package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.QueryDataSource
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.viewmodel.params.NoParametersModel
import tornadofx.label
import kotlin.reflect.KClass

open class EmptyEditor(
    dataSourceClass: KClass<out QueryDataSource<out DataAccessObject>>
) : QueryParametersEditor<Unit, NoParametersModel>(
    dataSourceClass,
    NoParametersModel(),
    fields = {
        label("Этот запрос не имеет параметров")
    })