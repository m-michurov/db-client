package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.CompetitionsByFacilityDataSource
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.db.data.table.SportsFacilityDataSource
import me.mmichurov.dbclient.db.model.Competition
import me.mmichurov.dbclient.db.viewmodel.params.FacilitySportParameterModel
import tornadofx.*

/* Query #8 */
class CompetitionByFacilityQuery : Query<
        Competition,
        FacilitySportParameterModel.FacilitySportParameter,
        FacilitySportParameterModel,
        CompetitionsByFacilityDataSource>(
    "Соревнования, проведенные в указанном спортивном сооружении",
    CompetitionsByFacilityParametersEditor::class,
    CompetitionsByFacilityDataSource::class,
    columns = {
        column("Название", Competition::name)
        column("Дата проведения", Competition::date)
        column("Вид спорта", Competition::sport)
        column("Организатор", Competition::organizer)
    }
) {

    class CompetitionsByFacilityParametersEditor : QueryParametersEditor<
            FacilitySportParameterModel.FacilitySportParameter,
            FacilitySportParameterModel>(
        CompetitionsByFacilityDataSource::class,
        FacilitySportParameterModel(),
        fields = { model ->
            field("Место проведения").combobox(model.facility, values = find<SportsFacilityDataSource>().items) {
                required()
            }
            field("Вид спорта") {
                combobox(model.sport, values = find<SportsDataSource>().items) {
                    useMaxWidth = true
                }
                button("✖") {
                    tooltip("Очистить")
                    action { model.sport.value = null }
                }
            }
        }
    ) {

        override fun onDock() {
            find<SportsFacilityDataSource>().reload()
            find<SportsDataSource>().reload()
            super.onDock()
        }
    }
}