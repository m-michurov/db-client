package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.SportsmenBySportDataSource
import me.mmichurov.dbclient.db.data.table.RankDataSource
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.db.model.SportsmanRank
import me.mmichurov.dbclient.db.viewmodel.params.SportRankParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.SportRankParameterModel.SportRankParameter
import tornadofx.*


/* Query #2 */
class SportsmenBySportQuery : Query<
        SportsmanRank,
        SportRankParameter,
        SportRankParameterModel,
        SportsmenBySportDataSource>(
    "Спортсмены, занимающиеся указанным видом спорта",
    SportsmenBySportParametersEditor::class,
    SportsmenBySportDataSource::class,
    columns = {
        column("Спортсмен", SportsmanRank::sportsman)
        column("Разряд", SportsmanRank::rank)
    }
) {

    class SportsmenBySportParametersEditor : QueryParametersEditor<SportRankParameter, SportRankParameterModel>(
        SportsmenBySportDataSource::class,
        find(),
        fields = { model ->
            field("Вид спорта").combobox(model.sport, values = find<SportsDataSource>().items) {
                useMaxWidth = true
                required()
            }
            field("Разряд\nне ниже") {
                combobox(model.rank, values = find<RankDataSource>().items)
                button("✖") {
                    tooltip("Очистить")
                    action { model.rank.value = null }
                }
            }
        }
    ) {

        override fun onDock() {
            find<RankDataSource>().reload()
            find<SportsDataSource>().reload()
            super.onDock()
        }
    }
}