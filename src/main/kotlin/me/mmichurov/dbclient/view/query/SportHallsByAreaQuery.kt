package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.SportHallsByAreaDataSource
import me.mmichurov.dbclient.db.model.SportsHall
import me.mmichurov.dbclient.db.viewmodel.params.IntParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.IntParameterModel.IntParameter
import me.mmichurov.dbclient.view.positiveNumbersOnly
import tornadofx.column
import tornadofx.field
import tornadofx.required
import tornadofx.textfield

/* Query #1 */
class SportHallsByAreaQuery : Query<SportsHall, IntParameter, IntParameterModel, SportHallsByAreaDataSource>(
    "Спортивные залы с указанной площадью",
    SportHallsByAreaParametersEditor::class,
    SportHallsByAreaDataSource::class,
    columns = {
        column("Адрес", SportsHall::address)
        column("Название", SportsHall::name)
        column("Площадь", SportsHall::area)
    }
) {

    class SportHallsByAreaParametersEditor : QueryParametersEditor<IntParameter, IntParameterModel>(
        SportHallsByAreaDataSource::class,
        IntParameterModel(),
        fields = { model ->
            field("Площадь не менее").textfield(model.value) {
                positiveNumbersOnly()
                required()
            }
        }
    )
}