package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.data.query.CompetitorsWithinTimePeriodDataSource
import me.mmichurov.dbclient.db.model.ClubMembersCount
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel
import me.mmichurov.dbclient.db.viewmodel.params.DateRangeParameterModel.DateRangeParameter
import tornadofx.*


/* Query #9 */
class CompetitorsWithinTimePeriodQuery : Query<
        ClubMembersCount,
        DateRangeParameter,
        DateRangeParameterModel,
        CompetitorsWithinTimePeriodDataSource>(
    "Число спортсменов, участвовавших в соревнованиях в течение указанного периода времени",
    CompetitorsWithinTimePeriodParametersEditor::class,
    CompetitorsWithinTimePeriodDataSource::class,
    columns = {
        column("Спортивный клуб", ClubMembersCount::club)
        column("Число участников соревнований", ClubMembersCount::members)
    }
) {

    class CompetitorsWithinTimePeriodParametersEditor : QueryParametersEditor<
            DateRangeParameter,
            DateRangeParameterModel>(
        CompetitorsWithinTimePeriodDataSource::class,
        DateRangeParameterModel(),
        fields = { model ->
            field("С").datepicker(model.startDate) {
                useMaxWidth = true
                required()
            }
            field("До").datepicker(model.endDate) {
                useMaxWidth = true
                required()
            }
        }
    )
}