package me.mmichurov.dbclient.view.query

import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.db.data.query.CoachesBySportDataSource
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.db.viewmodel.params.SportParameterModel.SportParameter
import me.mmichurov.dbclient.db.viewmodel.params.SportParameterModel
import tornadofx.*

/* Query #10 */
class CoachesBySportQuery : Query<Coach, SportParameter, SportParameterModel, CoachesBySportDataSource>(
    "Тренеры по определенному виду спорта",
    CoachesBySportParametersEditor::class,
    CoachesBySportDataSource::class,
    columns = {
        column("Имя", Coach::firstName)
        column("Фамилия", Coach::lastName)
        column("Отчество", Coach::patronymic)
    }
) {

    class CoachesBySportParametersEditor : QueryParametersEditor<SportParameter, SportParameterModel>(
        CoachesBySportDataSource::class,
        find(),
        fields = { model ->
            field("Вид спорта").combobox(model.sport, values = find<SportsDataSource>().items) {
                required()
            }
        }
    ) {

        override fun onDock() = find<SportsDataSource>().reload()
    }
}