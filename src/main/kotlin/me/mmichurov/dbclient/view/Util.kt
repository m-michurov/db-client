package me.mmichurov.dbclient.view

import javafx.scene.control.Alert
import javafx.scene.control.TextFormatter
import javafx.scene.control.TextInputControl
import javafx.scene.layout.Pane
import javafx.scene.layout.Region
import tornadofx.*

fun errorPopup(what: String, throwable: Throwable) {
    throwable.printStackTrace()
    alert(
        Alert.AlertType.ERROR,
        title = "Ошибка",
        header = what,
        content = throwable.message
    )
}

fun TextInputControl.maxLength(maxLength: Int) {
    validator { content ->
        content ?: return@validator null
        if (content.toByteArray().size > maxLength) {
            error("Для поля установлено ограничение в $maxLength байт")
        } else null
    }
}

fun TextInputControl.positiveNumbersOnly() {
    textFormatter = TextFormatter<Int> {
        when {
            it.isReplaced or it.isAdded -> when {
                it.text.toIntOrNull() == null -> null
                it.text.toInt() <= 0 && text.isBlank() -> null
                else -> it
            }
            else -> it
        }
    }
}

fun Pane.setMaxWidthForChildren() {
    getChildList()?.forEach { (it as Region).useMaxWidth = true }
}

fun Pane.notFocusTraversable() {
    getChildList()?.forEach { it.isFocusTraversable = false }
}
