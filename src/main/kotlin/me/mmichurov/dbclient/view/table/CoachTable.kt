package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.Coach
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.db.data.table.CoachDataSource
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.db.viewmodel.data.CoachModel
import tornadofx.*

fun fields(fieldset: Fieldset, model: CoachModel) = fieldset.run {
    val first = field("Имя").textfield(model.firstName) {
        required()
        maxLength(25)
    }
    field("Фамилия").textfield(model.lastName) {
        required()
        maxLength(25)
    }
    field("Отчество").textfield(model.patronymic) {
        maxLength(25)
    }
    field("Вид спорта").combobox(model.sport, values = find<SportsDataSource>().items) {
        required()
    }
    first
}

class CoachCreator : CreatorForm<Coach, CoachModel, CoachDataSource>(
    "Добавить тренера",
    CoachDataSource::class,
    CoachModel(Coach()),
    ::fields
) {

    override fun onDock() = find<SportsDataSource>().reload()
}

class CoachEditor : EditorForm<Coach, CoachModel, CoachDataSource>(
    "Редактировать информацию о тренере",
    CoachDataSource::class,
    find(),
    ::fields
) {

    override fun onDock() = find<SportsDataSource>().reload()
}

class CoachTable : Table<Coach, CoachModel, CoachDataSource>(
    "Тренеры",
    CoachCreator::class,
    CoachEditor::class,
    CoachDataSource::class,
    CoachModel::class,
    columns = {
        column("Имя", Coach::firstName)
        column("Фамилия", Coach::lastName)
        column("Отчество", Coach::patronymic)
        column("Вид спорта", Coach::sport)
    })