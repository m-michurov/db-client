package me.mmichurov.dbclient.view.table

import javafx.geometry.Pos
import me.mmichurov.dbclient.db.data.table.CompetitionDataSource
import me.mmichurov.dbclient.db.data.table.CompetitionOrganizerDataSource
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.db.data.table.SportsFacilityDataSource
import me.mmichurov.dbclient.db.model.Competition
import me.mmichurov.dbclient.db.viewmodel.data.CompetitionModel
import me.mmichurov.dbclient.db.viewmodel.params.CompetitionParameterModel
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.view.query.modal.CompetitionParticipantsModalQuery
import tornadofx.*

fun fields(fs: Fieldset, model: CompetitionModel) = fs.run {
    val first = field("Название").textfield(model.name) {
        required()
        maxLength(150)
    }
    field("Дата проведения").datepicker(model.date) {
        required()
    }
    field("Вид спорта").combobox(model.sport, values = find<SportsDataSource>().items) {
        required()
    }
    field("Организатор").combobox(model.organizer, values = find<CompetitionOrganizerDataSource>().items) {
        required()
    }
    field("Место проведения").combobox(model.place, values = find<SportsFacilityDataSource>().items) {
        required()
    }
    first
}

class CompetitionCreator : CreatorForm<Competition, CompetitionModel, CompetitionDataSource>(
    "Добавить соревнование",
    CompetitionDataSource::class,
    CompetitionModel(Competition()),
    ::fields
) {

    override fun onDock() {
        find<SportsDataSource>().reload()
        find<CompetitionOrganizerDataSource>().reload()
        find<SportsFacilityDataSource>().reload()
    }
}

class CompetitionEditor : EditorForm<Competition, CompetitionModel, CompetitionDataSource>(
    "Редактировать информацию о соревновании",
    CompetitionDataSource::class,
    find(),
    ::fields
) {

    override fun onDock() {
        find<SportsDataSource>().reload()
        find<CompetitionOrganizerDataSource>().reload()
        find<SportsFacilityDataSource>().reload()
    }
}

class CompetitionTable : Table<Competition, CompetitionModel, CompetitionDataSource>(
    "Соревнования",
    CompetitionCreator::class,
    CompetitionEditor::class,
    CompetitionDataSource::class,
    CompetitionModel::class,
    columns = {
        column("Название", Competition::name)
        column("Дата проведения", Competition::date)
        column("Вид спорта", Competition::sport)
        column("Организатор", Competition::organizer)
        column("Место проведения", Competition::place)
    },
    sidebar = { table, selected ->
        label("Связанное").alignment = Pos.CENTER
        separator()
        button("Участники") {
            tooltip("Список участников выбранного соревнования")
            enableWhen(!selected.empty)
            action {
                table.openInternalWindow(
                    CompetitionParticipantsModalQuery(
                        "Участники соревнования ${selected.item}",
                        CompetitionParameterModel(selected.item)
                    )
                )
            }
        }
    })