package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.data.table.CourtDataSource
import me.mmichurov.dbclient.db.model.Court
import me.mmichurov.dbclient.db.viewmodel.data.CourtModel
import me.mmichurov.dbclient.view.maxLength
import tornadofx.*

fun fields(fs: Fieldset, model: CourtModel) = fs.run {
    val first = field("Адрес").textfield(model.address) {
        required()
        maxLength(150)
    }
    field("Название").textfield(model.name) {
        required()
        maxLength(50)
    }
    field("Тип покрытия").combobox(model.surfaceType, values = Court.SURFACE_TYPES) {
        required()
    }
    first
}


class CourtCreator : CreatorForm<Court, CourtModel, CourtDataSource>(
    "Добавить корт",
    CourtDataSource::class,
    CourtModel(Court()),
    ::fields
)

class CourtEditor : EditorForm<Court, CourtModel, CourtDataSource>(
    "Редактировать информацию о корте",
    CourtDataSource::class,
    find(),
    ::fields
)

class CourtTable : Table<Court, CourtModel, CourtDataSource>(
    "Корты",
    CourtCreator::class,
    CourtEditor::class,
    CourtDataSource::class,
    CourtModel::class,
    columns = {
        column("Адрес", Court::address)
        column("Название", Court::name)
        column("Тип покрытия", Court::surfaceType)
    })