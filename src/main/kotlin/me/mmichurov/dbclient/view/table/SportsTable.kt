package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.Sport
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.db.viewmodel.data.SportModel
import tornadofx.*

fun fields(fs: Fieldset, model: SportModel) = fs.run {
    field("Название").textfield(model.name) {
        required()
        maxLength(65)
    }
}


class SportCreator : CreatorForm<Sport, SportModel, SportsDataSource>(
    "Добавить вид спорта",
    SportsDataSource::class,
    SportModel(Sport()),
    ::fields
)

class SportEditor : EditorForm<Sport, SportModel, SportsDataSource>(
    "Редактировать вид спорта",
    SportsDataSource::class,
    find(),
    ::fields
)

class SportsTable : Table<Sport, SportModel, SportsDataSource>(
    "Виды спорта",
    SportCreator::class,
    SportEditor::class,
    SportsDataSource::class,
    SportModel::class,
    columns = {
        column("Название", Sport::name)
    })