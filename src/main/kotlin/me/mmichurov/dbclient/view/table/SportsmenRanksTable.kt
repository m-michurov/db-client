package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.SportsmanRank
import me.mmichurov.dbclient.db.data.table.*
import me.mmichurov.dbclient.db.viewmodel.data.SportsmanRankModel
import tornadofx.*

fun fields(fs: Fieldset, model: SportsmanRankModel) = fs.run {
    val first = field("Разряд").combobox(model.rank, values = find<RankDataSource>().items) {
        required()
    }
    field("Вид спорта").combobox(model.sport, values = find<SportsDataSource>().items) {
        required()
    }
    field("Спортсмен").combobox(model.sportsman, values = find<SportsmanDataSource>().items) {
        required()
    }
    first
}

class SportsmanRankCreator : CreatorForm<SportsmanRank, SportsmanRankModel, SportsmenRanksDataSource>(
    "Добавить спортивный разряд",
    SportsmenRanksDataSource::class,
    SportsmanRankModel(SportsmanRank()),
    ::fields
) {

    override fun onDock() {
        find<RankDataSource>().reload()
        find<SportsDataSource>().reload()
        find<SportsmanDataSource>().reload()
    }
}


class SportsmenRanksTable : Table<SportsmanRank, SportsmanRankModel, SportsmenRanksDataSource>(
    "Спортивные разряды спортсменов",
    SportsmanRankCreator::class,
    null,
    SportsmenRanksDataSource::class,
    SportsmanRankModel::class,
    columns = {
        column("Разряд", SportsmanRank::rank)
        column("Вид спорта", SportsmanRank::sport)
        column("Спортсмен", SportsmanRank::sportsman)
    })
