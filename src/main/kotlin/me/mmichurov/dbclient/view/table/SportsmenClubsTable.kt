package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.SportsmanClub
import me.mmichurov.dbclient.db.data.table.SportClubDataSource
import me.mmichurov.dbclient.db.data.table.SportsmanDataSource
import me.mmichurov.dbclient.db.data.table.SportsmenClubsDataSource
import me.mmichurov.dbclient.db.viewmodel.data.SportsmanClubModel
import tornadofx.*

fun fields(fs: Fieldset, model: SportsmanClubModel) = fs.run {
    val first = field("Клуб").combobox(model.club, values = find<SportClubDataSource>().items) {
        required()
    }
    field("Спортсмен").combobox(model.sportsman, values = find<SportsmanDataSource>().items) {
        required()
    }
    first
}

class SportsmanClubCreator : CreatorForm<SportsmanClub, SportsmanClubModel, SportsmenClubsDataSource>(
    "Добавить члена спортивного клуба",
    SportsmenClubsDataSource::class,
    SportsmanClubModel(SportsmanClub()),
    ::fields
) {

    override fun onDock() {
        find<SportClubDataSource>().reload()
        find<SportsmanDataSource>().reload()
    }
}


class SportsmenClubsTable : Table<SportsmanClub, SportsmanClubModel, SportsmenClubsDataSource>(
    "Члены спортивных клубов",
    SportsmanClubCreator::class,
    null,
    SportsmenClubsDataSource::class,
    SportsmanClubModel::class,
    columns = {
        column("Клуб", SportsmanClub::club)
        column("Спортсмен", SportsmanClub::sportsman)
    })