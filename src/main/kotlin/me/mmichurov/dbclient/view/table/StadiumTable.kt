package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.Stadium
import me.mmichurov.dbclient.db.data.table.StadiumDataSource
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.view.positiveNumbersOnly
import me.mmichurov.dbclient.db.viewmodel.data.StadiumModel
import tornadofx.*

fun fields(fs: Fieldset, model: StadiumModel) = fs.run {
    val first = field("Адрес").textfield(model.address) {
        required()
        maxLength(150)
    }
    field("Название").textfield(model.name) {
        required()
        maxLength(50)
    }
    field("Вместимость").textfield(model.capacity) {
        required()
        positiveNumbersOnly()
    }
    first
}


class StadiumCreator : CreatorForm<Stadium, StadiumModel, StadiumDataSource>(
    "Добавить стадион",
    StadiumDataSource::class,
    StadiumModel(Stadium()),
    ::fields
)

class StadiumEditor : EditorForm<Stadium, StadiumModel, StadiumDataSource>(
    "Редактировать информацию о стадионе",
    StadiumDataSource::class,
    find(),
    ::fields
)

class StadiumTable : Table<Stadium, StadiumModel, StadiumDataSource>(
    "Стадионы",
    StadiumCreator::class,
    StadiumEditor::class,
    StadiumDataSource::class,
    StadiumModel::class,
    columns = {
        column("Адрес", Stadium::address)
        column("Название", Stadium::name)
        column("Вместимость", Stadium::capacity)
    })