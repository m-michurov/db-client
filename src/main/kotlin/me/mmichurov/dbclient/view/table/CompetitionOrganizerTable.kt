package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.CompetitionOrganizer
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.db.data.table.CompetitionOrganizerDataSource
import me.mmichurov.dbclient.db.viewmodel.data.CompetitionOrganizerModel
import tornadofx.*

fun fields(fs: Fieldset, model: CompetitionOrganizerModel) = fs.run {
    field("Название").textfield(model.organizationName) {
        required()
        maxLength(65)
    }
}


class CompetitionOrganizerCreator :
    CreatorForm<CompetitionOrganizer, CompetitionOrganizerModel, CompetitionOrganizerDataSource>(
        "Добавить организатора",
        CompetitionOrganizerDataSource::class,
        CompetitionOrganizerModel(CompetitionOrganizer()),
        ::fields
    )

class CompetitionOrganizerEditor :
    EditorForm<CompetitionOrganizer, CompetitionOrganizerModel, CompetitionOrganizerDataSource>(
        "Редактировать информацию об организаторе",
        CompetitionOrganizerDataSource::class,
        find(),
        ::fields
    )

class CompetitionOrganizerTable :
    Table<CompetitionOrganizer, CompetitionOrganizerModel, CompetitionOrganizerDataSource>(
        "Организаторы соревнований",
        CompetitionOrganizerCreator::class,
        CompetitionOrganizerEditor::class,
        CompetitionOrganizerDataSource::class,
        CompetitionOrganizerModel::class,
        columns = {
            column("Название организации", CompetitionOrganizer::organizationName)
        })