package me.mmichurov.dbclient.view.table

import javafx.scene.control.TableView
import javafx.scene.layout.Pane
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.data.table.TableDataSource
import me.mmichurov.dbclient.view.setMaxWidthForChildren
import me.mmichurov.dbclient.view.errorPopup
import me.mmichurov.dbclient.view.notFocusTraversable
import me.mmichurov.dbclient.db.viewmodel.data.TableEntryModel
import me.mmichurov.dbclient.view.Main
import tornadofx.*
import kotlin.reflect.KClass

abstract class Table<E : DataAccessObject, M : TableEntryModel<E>, C : TableDataSource<E>>(
    name: String,
    creatorClass: KClass<out TableEntryForm<E, out TableEntryModel<E>, C>>,
    editorClass: KClass<out TableEntryForm<E, M, C>>?,
    controllerClass: KClass<C>,
    modelClass: KClass<M>,
    columns: TableView<E>.() -> Unit,
    sidebar: Pane.(Table<E, M, C>, M) -> Unit = { _, _ -> }
) : View(name) {

    private val controller = find(controllerClass)
    private val creator = find(creatorClass)
    private val editor = editorClass?.let { find(it) }
    private val selected = find(modelClass)

    private val readStatus = TaskStatus()
    private val deleteStatus = TaskStatus()

    override val root = borderpane {
        center = vbox {
            tableview(controller.items) {
                prefHeightProperty().bind(primaryStage.heightProperty())
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY

                columns()

                onDoubleClick { update() }
                editModel.tableView.bindSelected(selected)
            }
        }

        right = borderpane {
            top = vbox {
                spacing = 1.0
                button("Обновить") {
                    disableWhen(readStatus.running)
                    action { read() }
                }
                separator()
                button("Добавить запись") {
                    action { create() }
                }
                editor?.let {
                    button("Редактировать") {
                        disableWhen(selected.empty)
                        action { update() }
                    }
                }
                button("Удалить") {
                    disableWhen(selected.empty or deleteStatus.running)
                    action { delete() }
                }
                separator()
                sidebar(this@Table, selected)
                setMaxWidthForChildren()
                notFocusTraversable()
            }

            bottom = vbox {
                spacing = 1.0
                separator()
                button("Назад") {
                    action { replaceWith<Main>() }
                }
                setMaxWidthForChildren()
                notFocusTraversable()
            }
        }
    }

    private fun create() {
        open(creator)
    }

    private fun read() {
        runAsync(readStatus) { controller.reload() } fail { errorPopup("Не удалось загрузить данные", it) }
    }

    private fun update() {
        if (selected.isEmpty) return
        editor?.let { open(it) }
    }

    private fun delete() {
        runAsync(deleteStatus) {
            controller.delete(selected.item)
        } fail {
            errorPopup("Не удалось удалить запись", it)
        }
    }

    private fun open(view: TableEntryForm<E, out TableEntryModel<E>, C>) {
        openInternalWindow(view)
        view.requestFocus()
    }

    override fun onDock() {
        super.onDock()
        read()
    }
}