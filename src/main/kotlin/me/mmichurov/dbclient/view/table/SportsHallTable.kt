package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.data.table.SportsHallDataSource
import me.mmichurov.dbclient.db.model.Stadium
import me.mmichurov.dbclient.db.data.table.StadiumDataSource
import me.mmichurov.dbclient.db.model.SportsHall
import me.mmichurov.dbclient.db.viewmodel.data.SportsHallModel
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.view.positiveNumbersOnly
import me.mmichurov.dbclient.db.viewmodel.data.StadiumModel
import tornadofx.*

fun fields(fs: Fieldset, model: SportsHallModel) = fs.run {
    val first = field("Адрес").textfield(model.address) {
        required()
        maxLength(150)
    }
    field("Название").textfield(model.name) {
        required()
        maxLength(50)
    }
    field("Площадь").textfield(model.area) {
        required()
        positiveNumbersOnly()
    }
    first
}


class SportsHallCreator : CreatorForm<SportsHall, SportsHallModel, SportsHallDataSource>(
    "Добавить спортивный зал",
    SportsHallDataSource::class,
    SportsHallModel(SportsHall()),
    ::fields
)

class SportsHallEditor : EditorForm<SportsHall, SportsHallModel, SportsHallDataSource>(
    "Редактировать информацию о спортивном зале",
    SportsHallDataSource::class,
    find(),
    ::fields
)

class SportsHallTable : Table<SportsHall, SportsHallModel, SportsHallDataSource>(
    "Спортивные залы",
    SportsHallCreator::class,
    SportsHallEditor::class,
    SportsHallDataSource::class,
    SportsHallModel::class,
    columns = {
        column("Адрес", SportsHall::address)
        column("Название", SportsHall::name)
        column("Площадь", SportsHall::area)
    })