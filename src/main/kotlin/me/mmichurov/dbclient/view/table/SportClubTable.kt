package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.SportClub
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.db.data.table.SportClubDataSource
import me.mmichurov.dbclient.db.data.table.SportsDataSource
import me.mmichurov.dbclient.db.viewmodel.data.SportClubModel
import tornadofx.*

fun fields(fs: Fieldset, model: SportClubModel) = fs.run {
    val first = field("Название").textfield(model.name) {
        required()
        maxLength(100)
    }
    field("Вид спорта").combobox(model.sport, values = find<SportsDataSource>().items) {
        required()
    }
    first
}

class SportClubCreator : CreatorForm<SportClub, SportClubModel, SportClubDataSource>(
    "Добавить спортивный клуб",
    SportClubDataSource::class,
    SportClubModel(SportClub()),
    ::fields
) {

    override fun onDock() = find<SportsDataSource>().reload()
}

class SportClubEditor : EditorForm<SportClub, SportClubModel, SportClubDataSource>(
    "Редактировать информацию о спортивном клубе",
    SportClubDataSource::class,
    find(),
    ::fields
) {

    override fun onDock() = find<SportsDataSource>().reload()
}

class SportClubTable : Table<SportClub, SportClubModel, SportClubDataSource>(
    "Спортивные клубы",
    SportClubCreator::class,
    SportClubEditor::class,
    SportClubDataSource::class,
    SportClubModel::class,
    columns = {
        column("Название", SportClub::name)
        column("Вид спорта", SportClub::sport)
    })