package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.data.table.CompetitionDataSource
import me.mmichurov.dbclient.db.data.table.CompetitorsDataSource
import me.mmichurov.dbclient.db.data.table.SportsmanDataSource
import me.mmichurov.dbclient.db.model.Competitor
import me.mmichurov.dbclient.db.viewmodel.data.CompetitorModel
import me.mmichurov.dbclient.db.viewmodel.data.TableEntryModel
import tornadofx.*

fun fields(fs: Fieldset, model: CompetitorModel) = fs.run {
    val first = field("Соревнование").combobox(model.competition, values = find<CompetitionDataSource>().items) {
        required()
    }
    field("Спортсмен").combobox(model.sportsman, values = find<SportsmanDataSource>().items) {
        required()
    }
    field("Место").combobox(model.place, values = Competitor.PLACES)
    first
}

class CompetitorCreator : CreatorForm<Competitor, CompetitorModel, CompetitorsDataSource>(
    "Добавить участника",
    CompetitorsDataSource::class,
    CompetitorModel(Competitor()),
    ::fields
) {

    override fun onDock() {
        find<CompetitionDataSource>().reload()
        find<SportsmanDataSource>().reload()
    }
}

class CompetitorEditor : EditorForm<Competitor, CompetitorEditor.CompetitorPlaceModel, CompetitorsDataSource>(
    "Изменить место",
    CompetitorsDataSource::class,
    find(),
    { model -> field("Место").combobox(model.place, values = Competitor.PLACES) }
) {

    override fun onDock() {
        find<CompetitionDataSource>().reload()
        find<SportsmanDataSource>().reload()
    }

    class CompetitorPlaceModel : TableEntryModel<Competitor>(null) {

        var place = bind(Competitor::place)

        override fun clear() {
            place.value = null
        }
    }
}


class CompetitorsTable : Table<Competitor, CompetitorEditor.CompetitorPlaceModel, CompetitorsDataSource>(
    "Участники соревнований",
    CompetitorCreator::class,
    CompetitorEditor::class,
    CompetitorsDataSource::class,
    CompetitorEditor.CompetitorPlaceModel::class,
    columns = {
        column("Соревнование", Competitor::competition)
        column("Спортсмен", Competitor::sportsman)
        column("Место", Competitor::place)
    })
