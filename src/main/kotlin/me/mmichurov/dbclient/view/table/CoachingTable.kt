package me.mmichurov.dbclient.view.table

import me.mmichurov.dbclient.db.model.Coaching
import me.mmichurov.dbclient.db.data.table.CoachDataSource
import me.mmichurov.dbclient.db.data.table.CoachingDataSource
import me.mmichurov.dbclient.db.data.table.SportsmanDataSource
import me.mmichurov.dbclient.db.viewmodel.data.CoachingModel
import tornadofx.*

fun fields(fs: Fieldset, model: CoachingModel) = fs.run {
    val first = field("Тренер").combobox(model.coach, values = find<CoachDataSource>().items) {
        required()
    }
    field("Спортсмен").combobox(model.sportsman, values = find<SportsmanDataSource>().items) {
        required()
    }
    first
}

class CoachingCreator : CreatorForm<Coaching, CoachingModel, CoachingDataSource>(
    "Добавить тренера спортсмена",
    CoachingDataSource::class,
    CoachingModel(Coaching()),
    ::fields
) {

    override fun onDock() {
        find<CoachDataSource>().reload()
        find<SportsmanDataSource>().reload()
    }
}


class CoachingTable : Table<Coaching, CoachingModel, CoachingDataSource>(
    "Тренеры спортсменов",
    CoachingCreator::class,
    null,
    CoachingDataSource::class,
    CoachingModel::class,
    columns = {
        column("Тренер", Coaching::coach)
        column("Спортсмен", Coaching::sportsman)
    })
