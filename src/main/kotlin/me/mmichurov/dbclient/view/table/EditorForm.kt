package me.mmichurov.dbclient.view.table

import javafx.scene.control.Control
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.db.data.table.TableDataSource
import me.mmichurov.dbclient.view.errorPopup
import me.mmichurov.dbclient.db.viewmodel.data.TableEntryModel
import tornadofx.Fieldset
import tornadofx.fail
import tornadofx.success
import kotlin.reflect.KClass

abstract class EditorForm<T : DataAccessObject, U : TableEntryModel<T>, V : TableDataSource<T>>(
    title: String,
    controllerClass: KClass<V>,
    model: U,
    fields: Fieldset.(U) -> Control
) : TableEntryForm<T, U, V>(title, "Сохранить", controllerClass, model, fields) {

    final override fun submit() {
        runAsync(submitStatus) {
            model.commit {
                controller.update(model.item)
            }
        } success {
            close()
        } fail {
            errorPopup("Не удалось сохранить запись", it)
        }
    }

    override fun onUndock() {
        model.rollback()
        super.onUndock()
    }
}