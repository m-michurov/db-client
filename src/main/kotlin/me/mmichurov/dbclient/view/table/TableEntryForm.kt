package me.mmichurov.dbclient.view.table

import javafx.beans.binding.BooleanBinding
import javafx.scene.control.Control
import me.mmichurov.dbclient.db.data.table.TableDataSource
import me.mmichurov.dbclient.db.viewmodel.data.TableEntryModel
import me.mmichurov.dbclient.db.model.DataAccessObject
import me.mmichurov.dbclient.view.setMaxWidthForChildren
import tornadofx.*
import kotlin.reflect.KClass

abstract class TableEntryForm<T : DataAccessObject, U : TableEntryModel<T>, V : TableDataSource<T>>(
    title: String,
    actionName: String,
    controllerClass: KClass<V>,
    internal val model: U,
    fields: Fieldset.(U) -> Control,
    enableWhenCondition: BooleanBinding? = null
) : View(title, icon = null) {

    protected val controller = find(controllerClass)
    protected val submitStatus = TaskStatus()
    private var firstField: Control by singleAssign()

    override val root = form {
        fieldset {
            firstField = fields(model)
        }
        buttonbar {
            button("Очистить все поля") {
                disableWhen(submitStatus.running)
                action { model.clear() }
            }
            button(actionName) {
                enableWhenCondition?.let {
                    enableWhen(model.dirty and model.valid and !submitStatus.running and it)
                } ?: run {
                    enableWhen(model.dirty and model.valid and !submitStatus.running)
                }
                action { submit() }
            }
            setMaxWidthForChildren()
        }
    }

    init {
        model.validate()
    }

    fun requestFocus() = firstField.requestFocus()

    protected abstract fun submit()
}

