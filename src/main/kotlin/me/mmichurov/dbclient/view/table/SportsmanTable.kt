package me.mmichurov.dbclient.view.table

import javafx.geometry.Pos
import me.mmichurov.dbclient.db.data.table.SportClubDataSource
import me.mmichurov.dbclient.db.data.table.SportsmanDataSource
import me.mmichurov.dbclient.db.model.Sportsman
import me.mmichurov.dbclient.db.viewmodel.data.SportsmanModel
import me.mmichurov.dbclient.db.viewmodel.params.SportsmanParameterModel
import me.mmichurov.dbclient.view.maxLength
import me.mmichurov.dbclient.view.query.modal.SportsmanClubsModalQuery
import me.mmichurov.dbclient.view.query.modal.SportsmanCoachesModalQuery
import me.mmichurov.dbclient.view.query.modal.SportsmanRanksModalQuery
import tornadofx.*

fun fields(fs: Fieldset, model: SportsmanModel) = fs.run {
    val first = field("Имя").textfield(model.firstName) {
        required()
        maxLength(25)
    }
    field("Фамилия").textfield(model.lastName) {
        required()
        maxLength(25)
    }
    field("Отчество").textfield(model.patronymic) {
        maxLength(25)
    }
    field("Дата рождения").datepicker(model.dateOfBirth)
    first
}

class SportsmanCreator : CreatorForm<Sportsman, SportsmanModel, SportsmanDataSource>(
    "Добавить спортсмена",
    SportsmanDataSource::class,
    SportsmanModel(Sportsman()),
    { model ->
        val first = fields(this, model)
        field("Спортивный клуб").combobox(model.club, values = find<SportClubDataSource>().items) {
            required(
                message = "Спортсмен должен быть членом хотя бы одного клуба\nВы сможете добавить больше клубов позже"
            )
        }
        first
    }
) {

    override fun onDock() = find<SportClubDataSource>().reload()
}

class SportsmanEditor : EditorForm<Sportsman, SportsmanModel, SportsmanDataSource>(
    "Редактировать информацию о спртсмене",
    SportsmanDataSource::class,
    find(),
    ::fields
)

class SportsmanTable : Table<Sportsman, SportsmanModel, SportsmanDataSource>(
    "Спортсмены",
    SportsmanCreator::class,
    SportsmanEditor::class,
    SportsmanDataSource::class,
    SportsmanModel::class,
    columns = {
        column("Имя", Sportsman::firstName)
        column("Фамилия", Sportsman::lastName)
        column("Отчество", Sportsman::patronymic)
        column("Дата рождения", Sportsman::dateOfBirth)
    },
    sidebar = { table, selected ->
        label("Связанное").alignment = Pos.CENTER
        separator()
        button("Спортивные разряды") {
            tooltip("Список спортивных разрядов выбранного спортсмена")
            enableWhen(!selected.empty)
            action {
                table.openInternalWindow(
                    SportsmanRanksModalQuery(
                        "Спортивные разряды спортсмена ${selected.item}",
                        SportsmanParameterModel(selected.item)
                    )
                )
            }
        }
        button("Тренеры") {
            tooltip("Список тренеров выбранного спортсмена")
            enableWhen(!selected.empty)
            action {
                table.openInternalWindow(
                    SportsmanCoachesModalQuery(
                        "Тренеры спортсмена ${selected.item}",
                        SportsmanParameterModel(selected.item)
                    )
                )
            }
        }
        button("Спортклубы") {
            tooltip("Список спортивных клубов выбранного спортсмена")
            enableWhen(!selected.empty)
            action {
                table.openInternalWindow(
                    SportsmanClubsModalQuery(
                        "Спортивные клубы спортсмена ${selected.item}",
                        SportsmanParameterModel(selected.item)
                    )
                )
            }
        }
    })
