import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.30"
    id("application")
    id("org.openjfx.javafxplugin") version "0.0.9"
}

group = "me.mmichurov.dbclient"
version = "1.0"

repositories {
    mavenCentral()
    maven(url="https://oss.sonatype.org/content/repositories/snapshots")
}

dependencies {
    implementation("no.tornado:tornadofx:2.0.0-SNAPSHOT")
    implementation("org.jfxtras:jmetro:11.6.14")

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation("org.slf4j:slf4j-nop:1.7.30")

    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
}

javafx {
    version = "15.0.1"
    modules("javafx.controls", "javafx.graphics")
}

tasks {
    test {
        useJUnitPlatform()
    }

    jar {
        onlyIf { false }
    }

    val fatJar = task("fatJar", type = Jar::class) {
        archiveBaseName.set("${project.name}-fat")
        manifest {
            attributes["Main-Class"] = "${project.group}.MainKt"
        }
        from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
        with(jar.get() as CopySpec)
    }

    build {
        dependsOn(fatJar)
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}
